
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2$
create_project: 2default:default2
00:00:022default:default2
00:00:062default:default2
325.4142default:default2
69.8632default:defaultZ17-268h px� 
>
Refreshing IP repositories
234*coregenZ19-234h px� 
�
 Loaded user IP repository '%s'.
1135*coregen2L
8g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip2default:defaultZ19-1700h px� 
�
�If you move the project, the path for repository '%s' may become invalid. A better location for the repostory would be in a path adjacent to the project. (Current project location is '%s'.)1680*coregen2L
8g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip2default:default2k
Wg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.runs/fir_0_synth_12default:defaultZ19-3656h px� 
|
"Loaded Vivado IP repository '%s'.
1332*coregen23
G:/vivado/Vivado/2018.3/data/ip2default:defaultZ19-2313h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2'
update_ip_catalog: 2default:default2
00:00:052default:default2
00:00:092default:default2
357.0432default:default2
31.6292default:defaultZ17-268h px� 
�
Command: %s
53*	vivadotcl2W
Csynth_design -top fir_0 -part xc7k325tfbg676-3 -mode out_of_context2default:defaultZ4-113h px� 
:
Starting synth_design
149*	vivadotclZ4-321h px� 
�
@Attempting to get a license for feature '%s' and/or device '%s'
308*common2
	Synthesis2default:default2
xc7k325t2default:defaultZ17-347h px� 
�
0Got license for feature '%s' and/or device '%s'
310*common2
	Synthesis2default:default2
xc7k325t2default:defaultZ17-349h px� 
�
%s*synth2�
wStarting RTL Elaboration : Time (s): cpu = 00:00:07 ; elapsed = 00:00:15 . Memory (MB): peak = 475.566 ; gain = 98.777
2default:defaulth px� 
�
synthesizing module '%s'%s4497*oasys2
fir_02default:default2
 2default:default2�
jg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/synth/fir_0.v2default:default2
572default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2
fir2default:default2
 2default:default2�
kg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir.v2default:default2
11322default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2,
fir_default_clock_driver2default:default2
 2default:default2�
kg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir.v2default:default2
11102default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2!
xlclockdriver2default:default2
 2default:default2�
xg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/xlclockdriver_rd.v2default:default2
222default:default8@Z8-6157h px� 
u
%s
*synth2]
I	Parameter log_2_period bound to: 32'sb00000000000000000000000000000001 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter period bound to: 32'sb00000000000000000000000000000001 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter use_bufg bound to: 0 - type: integer 
2default:defaulth p
x
� 
v
%s
*synth2^
J	Parameter pipeline_regs bound to: 32'sb00000000000000000000000000000101 
2default:defaulth p
x
� 
z
%s
*synth2b
N	Parameter max_pipeline_regs bound to: 32'sb00000000000000000000000000001000 
2default:defaulth p
x
� 
z
%s
*synth2b
N	Parameter num_pipeline_regs bound to: 32'sb00000000000000000000000000000101 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter factor bound to: 32'sb00000000000000000000000000000101 
2default:defaulth p
x
� 
z
%s
*synth2b
N	Parameter rem_pipeline_regs bound to: 32'sb00000000000000000000000000000001 
2default:defaulth p
x
� 
T
%s
*synth2<
(	Parameter trunc_period bound to: 1'b1 
2default:defaulth p
x
� 
u
%s
*synth2]
I	Parameter period_floor bound to: 32'sb00000000000000000000000000000010 
2default:defaulth p
x
� 
{
%s
*synth2c
O	Parameter power_of_2_counter bound to: 32'sb00000000000000000000000000000001 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter cnt_width bound to: 32'sb00000000000000000000000000000001 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter clk_for_ce_pulse_minus1 bound to: 1'b0 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter clk_for_ce_pulse_minus2 bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter clk_for_ce_pulse_minus_regs bound to: 1'b0 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2$
synth_reg_w_init2default:default2
 2default:default2�
xg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/synth_reg_w_init.v2default:default2
32default:default8@Z8-6157h px� 
Z
%s
*synth2B
.	Parameter width bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter init_index bound to: 0 - type: integer 
2default:defaulth p
x
� 
R
%s
*synth2:
&	Parameter init_value bound to: 1'b0 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2%
single_reg_w_init2default:default2
 2default:default2�
xg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/synth_reg_w_init.v2default:default2
452default:default8@Z8-6157h px� 
Z
%s
*synth2B
.	Parameter width bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter init_index bound to: 0 - type: integer 
2default:defaulth p
x
� 
R
%s
*synth2:
&	Parameter init_value bound to: 1'b0 
2default:defaulth p
x
� 
V
%s
*synth2>
*	Parameter init_index_val bound to: 1'b0 
2default:defaulth p
x
� 
N
%s
*synth26
"	Parameter result bound to: 1'b0 
2default:defaulth p
x
� 
R
%s
*synth2:
&	Parameter init_const bound to: 1'b0 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2
FDRE2default:default2
 2default:default2K
5G:/vivado/Vivado/2018.3/scripts/rt/data/unisim_comp.v2default:default2
39712default:default8@Z8-6157h px� 
L
%s
*synth24
 	Parameter INIT bound to: 1'b0 
2default:defaulth p
x
� 
U
%s
*synth2=
)	Parameter IS_C_INVERTED bound to: 1'b0 
2default:defaulth p
x
� 
U
%s
*synth2=
)	Parameter IS_D_INVERTED bound to: 1'b0 
2default:defaulth p
x
� 
U
%s
*synth2=
)	Parameter IS_R_INVERTED bound to: 1'b0 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
FDRE2default:default2
 2default:default2
12default:default2
12default:default2K
5G:/vivado/Vivado/2018.3/scripts/rt/data/unisim_comp.v2default:default2
39712default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2%
single_reg_w_init2default:default2
 2default:default2
22default:default2
12default:default2�
xg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/synth_reg_w_init.v2default:default2
452default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2$
synth_reg_w_init2default:default2
 2default:default2
32default:default2
12default:default2�
xg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/synth_reg_w_init.v2default:default2
32default:default8@Z8-6155h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2
clk_num_reg2default:default2�
xg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/xlclockdriver_rd.v2default:default2
792default:default8@Z8-6014h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2!
xlclockdriver2default:default2
 2default:default2
42default:default2
12default:default2�
xg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/xlclockdriver_rd.v2default:default2
222default:default8@Z8-6155h px� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
clockdriver2default:default2!
xlclockdriver2default:default2
72default:default2
52default:default2�
kg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir.v2default:default2
11212default:default8@Z8-350h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2,
fir_default_clock_driver2default:default2
 2default:default2
52default:default2
12default:default2�
kg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir.v2default:default2
11102default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2

fir_struct2default:default2
 2default:default2�
kg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir.v2default:default2
10872default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2
fir_fir2default:default2
 2default:default2�
kg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir.v2default:default2
72default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2 
fir_xladdsub2default:default2
 2default:default2�
g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir_entity_declarations.v2default:default2
1242default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter core_name0 bound to: fir_c_addsub_v12_0_i0 - type: string 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter a_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter a_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter a_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_in_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_in_bin_pt bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_in_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter c_out_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter c_out_bin_pt bound to: 4 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_out_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter b_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter b_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter b_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter s_width bound to: 33 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter s_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter s_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter rst_width bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter rst_bin_pt bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter rst_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter en_width bound to: 1 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter en_bin_pt bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter en_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_s_width bound to: 33 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter full_s_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter mode bound to: 1 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter extra_registers bound to: 0 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter latency bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter quantization bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter overflow bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_a_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter c_b_width bound to: 8 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_a_type bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_b_type bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_has_sclr bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_has_ce bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter c_latency bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter c_output_width bound to: 33 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter c_enable_rlocs bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_has_c_in bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_has_c_out bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_a_width bound to: 33 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_b_width bound to: 33 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter full_s_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2)
fir_c_addsub_v12_0_i02default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i0/synth/fir_c_addsub_v12_0_i0.vhd2default:default2
672default:default8@Z8-638h px� 
`
%s
*synth2H
4	Parameter C_VERBOSITY bound to: 0 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_XDEVICEFAMILY bound to: kintex7 - type: string 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_IMPLEMENTATION bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_A_WIDTH bound to: 33 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_B_WIDTH bound to: 33 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_OUT_WIDTH bound to: 33 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_CE_OVERRIDES_SCLR bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_A_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_B_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter C_LATENCY bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_ADD_MODE bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_B_CONSTANT bound to: 0 - type: integer 
2default:defaulth p
x
� 
}
%s
*synth2e
Q	Parameter C_B_VALUE bound to: 000000000000000000000000000000000 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_AINIT_VAL bound to: 0 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_SINIT_VAL bound to: 0 - type: string 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_CE_OVERRIDES_BYPASS bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_BYPASS_LOW bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_SCLR_OVERRIDES_SSET bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_C_IN bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_HAS_C_OUT bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_BORROW_LOW bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_HAS_CE bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_HAS_BYPASS bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_SCLR bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_SSET bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_HAS_SINIT bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2%
c_addsub_v12_0_122default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i0/hdl/c_addsub_v12_0_vh_rfs.vhd2default:default2
70262default:default2
U02default:default2%
c_addsub_v12_0_122default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i0/synth/fir_c_addsub_v12_0_i0.vhd2default:default2
1302default:default8@Z8-3491h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2)
fir_c_addsub_v12_0_i02default:default2
112default:default2
12default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i0/synth/fir_c_addsub_v12_0_i0.vhd2default:default2
672default:default8@Z8-256h px� 
�
synthesizing module '%s'%s4497*oasys2
align_input2default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2602default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 33 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter abs_delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2

extend_msb2default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 33 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2
sign_ext2default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 33 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
sign_ext2default:default2
 2default:default2
122default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2

extend_msb2default:default2
 2default:default2
132default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
align_input2default:default2
 2default:default2
142default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2602default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2 
convert_type2default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7752default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 33 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 33 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter quantization bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter overflow bound to: 1 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter fp_width bound to: 32'sb00000000000000000000000000100011 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter fp_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter fp_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter q_width bound to: 32'sb00000000000000000000000000100011 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter q_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter q_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2
trunc2default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
5392default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100011 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000100011 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
x
%s
*synth2`
L	Parameter abs_right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2.
extend_msb__parameterized02default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100011 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000100011 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2,
sign_ext__parameterized02default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100011 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000100011 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2,
sign_ext__parameterized02default:default2
 2default:default2
142default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2.
extend_msb__parameterized02default:default2
 2default:default2
142default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
trunc2default:default2
 2default:default2
152default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
5392default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2

wrap_arith2default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7542default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100011 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 33 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter result_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2
cast2default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
132default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100011 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 33 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
cast2default:default2
 2default:default2
162default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
132default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2

wrap_arith2default:default2
 2default:default2
172default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7542default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2(
cast__parameterized02default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
132default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 33 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000100011 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2(
cast__parameterized02default:default2
 2default:default2
172default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
132default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2 
convert_type2default:default2
 2default:default2
182default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7752default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2 
fir_xladdsub2default:default2
 2default:default2
192default:default2
12default:default2�
g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir_entity_declarations.v2default:default2
1242default:default8@Z8-6155h px� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
addsub2default:default2 
fir_xladdsub2default:default2
102default:default2
72default:default2�
kg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir.v2default:default2
892default:default8@Z8-350h px� 
�
synthesizing module '%s'%s4497*oasys20
fir_xladdsub__parameterized02default:default2
 2default:default2�
g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir_entity_declarations.v2default:default2
1242default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter core_name0 bound to: fir_c_addsub_v12_0_i1 - type: string 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter a_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter a_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter a_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_in_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_in_bin_pt bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_in_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter c_out_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter c_out_bin_pt bound to: 4 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_out_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter b_width bound to: 33 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter b_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter b_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter s_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter s_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter s_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter rst_width bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter rst_bin_pt bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter rst_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter en_width bound to: 1 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter en_bin_pt bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter en_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_s_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter full_s_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter mode bound to: 1 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter extra_registers bound to: 0 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter quantization bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter overflow bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_a_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter c_b_width bound to: 8 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_a_type bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_b_type bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_has_sclr bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_has_ce bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter c_latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter c_output_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter c_enable_rlocs bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_has_c_in bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_has_c_out bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_a_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_b_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter full_s_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2)
fir_c_addsub_v12_0_i12default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i1/synth/fir_c_addsub_v12_0_i1.vhd2default:default2
692default:default8@Z8-638h px� 
`
%s
*synth2H
4	Parameter C_VERBOSITY bound to: 0 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_XDEVICEFAMILY bound to: kintex7 - type: string 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_IMPLEMENTATION bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_A_WIDTH bound to: 34 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_B_WIDTH bound to: 34 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_OUT_WIDTH bound to: 34 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_CE_OVERRIDES_SCLR bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_A_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_B_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter C_LATENCY bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_ADD_MODE bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_B_CONSTANT bound to: 0 - type: integer 
2default:defaulth p
x
� 
~
%s
*synth2f
R	Parameter C_B_VALUE bound to: 0000000000000000000000000000000000 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_AINIT_VAL bound to: 0 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_SINIT_VAL bound to: 0 - type: string 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_CE_OVERRIDES_BYPASS bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_BYPASS_LOW bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_SCLR_OVERRIDES_SSET bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_C_IN bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_HAS_C_OUT bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_BORROW_LOW bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_HAS_CE bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_HAS_BYPASS bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_SCLR bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_SSET bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_HAS_SINIT bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2%
c_addsub_v12_0_122default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i0/hdl/c_addsub_v12_0_vh_rfs.vhd2default:default2
70262default:default2
U02default:default2%
c_addsub_v12_0_122default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i1/synth/fir_c_addsub_v12_0_i1.vhd2default:default2
1362default:default8@Z8-3491h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2)
fir_c_addsub_v12_0_i12default:default2
212default:default2
12default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i1/synth/fir_c_addsub_v12_0_i1.vhd2default:default2
692default:default8@Z8-256h px� 
�
synthesizing module '%s'%s4497*oasys2/
align_input__parameterized02default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2602default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter abs_delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2.
extend_msb__parameterized12default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2,
sign_ext__parameterized12default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2,
sign_ext__parameterized12default:default2
 2default:default2
212default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2.
extend_msb__parameterized12default:default2
 2default:default2
212default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2/
align_input__parameterized02default:default2
 2default:default2
212default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2602default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2/
align_input__parameterized12default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2602default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 33 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter abs_delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2.
extend_msb__parameterized22default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 33 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2,
sign_ext__parameterized22default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 33 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2,
sign_ext__parameterized22default:default2
 2default:default2
212default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2.
extend_msb__parameterized22default:default2
 2default:default2
212default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2/
align_input__parameterized12default:default2
 2default:default2
212default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2602default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys20
convert_type__parameterized02default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7752default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter quantization bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter overflow bound to: 1 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter fp_width bound to: 32'sb00000000000000000000000000100100 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter fp_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter fp_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter q_width bound to: 32'sb00000000000000000000000000100100 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter q_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter q_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2)
trunc__parameterized02default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
5392default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100100 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000100100 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
x
%s
*synth2`
L	Parameter abs_right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2.
extend_msb__parameterized32default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100100 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000100100 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2,
sign_ext__parameterized32default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100100 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000100100 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2,
sign_ext__parameterized32default:default2
 2default:default2
212default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2.
extend_msb__parameterized32default:default2
 2default:default2
212default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2)
trunc__parameterized02default:default2
 2default:default2
212default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
5392default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2.
wrap_arith__parameterized02default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7542default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100100 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter result_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2(
cast__parameterized12default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
132default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100100 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2(
cast__parameterized12default:default2
 2default:default2
212default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
132default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2.
wrap_arith__parameterized02default:default2
 2default:default2
212default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7542default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2(
cast__parameterized22default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
132default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000100100 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2(
cast__parameterized22default:default2
 2default:default2
212default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
132default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys20
convert_type__parameterized02default:default2
 2default:default2
212default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7752default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys20
fir_xladdsub__parameterized02default:default2
 2default:default2
212default:default2
12default:default2�
g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir_entity_declarations.v2default:default2
1242default:default8@Z8-6155h px� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
addsub12default:default2 
fir_xladdsub2default:default2
102default:default2
72default:default2�
kg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir.v2default:default2
1192default:default8@Z8-350h px� 
�
synthesizing module '%s'%s4497*oasys20
fir_xladdsub__parameterized12default:default2
 2default:default2�
g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir_entity_declarations.v2default:default2
1242default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter core_name0 bound to: fir_c_addsub_v12_0_i2 - type: string 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter a_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter a_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter a_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_in_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_in_bin_pt bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_in_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter c_out_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter c_out_bin_pt bound to: 4 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_out_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter b_width bound to: 42 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter b_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter b_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter s_width bound to: 43 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter s_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter s_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter rst_width bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter rst_bin_pt bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter rst_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter en_width bound to: 1 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter en_bin_pt bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter en_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_s_width bound to: 43 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter full_s_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter mode bound to: 1 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter extra_registers bound to: 0 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter quantization bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter overflow bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_a_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter c_b_width bound to: 8 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_a_type bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_b_type bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_has_sclr bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_has_ce bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter c_latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter c_output_width bound to: 43 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter c_enable_rlocs bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_has_c_in bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_has_c_out bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_a_width bound to: 43 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_b_width bound to: 43 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter full_s_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2)
fir_c_addsub_v12_0_i22default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i2/synth/fir_c_addsub_v12_0_i2.vhd2default:default2
692default:default8@Z8-638h px� 
`
%s
*synth2H
4	Parameter C_VERBOSITY bound to: 0 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_XDEVICEFAMILY bound to: kintex7 - type: string 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_IMPLEMENTATION bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_A_WIDTH bound to: 43 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_B_WIDTH bound to: 43 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_OUT_WIDTH bound to: 43 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_CE_OVERRIDES_SCLR bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_A_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_B_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter C_LATENCY bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_ADD_MODE bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_B_CONSTANT bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
%s
*synth2o
[	Parameter C_B_VALUE bound to: 0000000000000000000000000000000000000000000 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_AINIT_VAL bound to: 0 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_SINIT_VAL bound to: 0 - type: string 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_CE_OVERRIDES_BYPASS bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_BYPASS_LOW bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_SCLR_OVERRIDES_SSET bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_C_IN bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_HAS_C_OUT bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_BORROW_LOW bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_HAS_CE bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_HAS_BYPASS bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_SCLR bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_SSET bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_HAS_SINIT bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2%
c_addsub_v12_0_122default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i0/hdl/c_addsub_v12_0_vh_rfs.vhd2default:default2
70262default:default2
U02default:default2%
c_addsub_v12_0_122default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i2/synth/fir_c_addsub_v12_0_i2.vhd2default:default2
1362default:default8@Z8-3491h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2)
fir_c_addsub_v12_0_i22default:default2
222default:default2
12default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i2/synth/fir_c_addsub_v12_0_i2.vhd2default:default2
692default:default8@Z8-256h px� 
�
synthesizing module '%s'%s4497*oasys2/
align_input__parameterized22default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2602default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 43 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter abs_delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2.
extend_msb__parameterized42default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 43 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2,
sign_ext__parameterized42default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 43 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2,
sign_ext__parameterized42default:default2
 2default:default2
222default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2.
extend_msb__parameterized42default:default2
 2default:default2
222default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2/
align_input__parameterized22default:default2
 2default:default2
222default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2602default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2/
align_input__parameterized32default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2602default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 42 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 43 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter abs_delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2.
extend_msb__parameterized52default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 42 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 43 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2,
sign_ext__parameterized52default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 42 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 43 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2,
sign_ext__parameterized52default:default2
 2default:default2
222default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2.
extend_msb__parameterized52default:default2
 2default:default2
222default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2/
align_input__parameterized32default:default2
 2default:default2
222default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2602default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys20
convert_type__parameterized12default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7752default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 43 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 43 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter quantization bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter overflow bound to: 1 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter fp_width bound to: 32'sb00000000000000000000000000101101 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter fp_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter fp_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter q_width bound to: 32'sb00000000000000000000000000101101 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter q_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter q_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2)
trunc__parameterized12default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
5392default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101101 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000101101 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
x
%s
*synth2`
L	Parameter abs_right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2.
extend_msb__parameterized62default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101101 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000101101 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2,
sign_ext__parameterized62default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101101 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000101101 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2,
sign_ext__parameterized62default:default2
 2default:default2
222default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2.
extend_msb__parameterized62default:default2
 2default:default2
222default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2)
trunc__parameterized12default:default2
 2default:default2
222default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
5392default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2.
wrap_arith__parameterized12default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7542default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101101 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 43 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter result_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2(
cast__parameterized32default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
132default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101101 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 43 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2(
cast__parameterized32default:default2
 2default:default2
222default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
132default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2.
wrap_arith__parameterized12default:default2
 2default:default2
222default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7542default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2(
cast__parameterized42default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
132default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 43 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000101101 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2(
cast__parameterized42default:default2
 2default:default2
222default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
132default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys20
convert_type__parameterized12default:default2
 2default:default2
222default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7752default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys20
fir_xladdsub__parameterized12default:default2
 2default:default2
222default:default2
12default:default2�
g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir_entity_declarations.v2default:default2
1242default:default8@Z8-6155h px� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
addsub102default:default2 
fir_xladdsub2default:default2
102default:default2
72default:default2�
kg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir.v2default:default2
1492default:default8@Z8-350h px� 
�
synthesizing module '%s'%s4497*oasys20
fir_xladdsub__parameterized22default:default2
 2default:default2�
g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir_entity_declarations.v2default:default2
1242default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter core_name0 bound to: fir_c_addsub_v12_0_i3 - type: string 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter a_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter a_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter a_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_in_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_in_bin_pt bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_in_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter c_out_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter c_out_bin_pt bound to: 4 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_out_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter b_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter b_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter b_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter s_width bound to: 35 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter s_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter s_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter rst_width bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter rst_bin_pt bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter rst_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter en_width bound to: 1 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter en_bin_pt bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter en_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_s_width bound to: 35 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter full_s_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter mode bound to: 1 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter extra_registers bound to: 0 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter quantization bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter overflow bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_a_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter c_b_width bound to: 8 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_a_type bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_b_type bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_has_sclr bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_has_ce bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter c_latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter c_output_width bound to: 35 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter c_enable_rlocs bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_has_c_in bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_has_c_out bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_a_width bound to: 35 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_b_width bound to: 35 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter full_s_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2)
fir_c_addsub_v12_0_i32default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i3/synth/fir_c_addsub_v12_0_i3.vhd2default:default2
692default:default8@Z8-638h px� 
`
%s
*synth2H
4	Parameter C_VERBOSITY bound to: 0 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_XDEVICEFAMILY bound to: kintex7 - type: string 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_IMPLEMENTATION bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_A_WIDTH bound to: 35 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_B_WIDTH bound to: 35 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_OUT_WIDTH bound to: 35 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_CE_OVERRIDES_SCLR bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_A_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_B_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter C_LATENCY bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_ADD_MODE bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_B_CONSTANT bound to: 0 - type: integer 
2default:defaulth p
x
� 

%s
*synth2g
S	Parameter C_B_VALUE bound to: 00000000000000000000000000000000000 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_AINIT_VAL bound to: 0 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_SINIT_VAL bound to: 0 - type: string 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_CE_OVERRIDES_BYPASS bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_BYPASS_LOW bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_SCLR_OVERRIDES_SSET bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_C_IN bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_HAS_C_OUT bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_BORROW_LOW bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_HAS_CE bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_HAS_BYPASS bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_SCLR bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_SSET bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_HAS_SINIT bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2%
c_addsub_v12_0_122default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i0/hdl/c_addsub_v12_0_vh_rfs.vhd2default:default2
70262default:default2
U02default:default2%
c_addsub_v12_0_122default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i3/synth/fir_c_addsub_v12_0_i3.vhd2default:default2
1362default:default8@Z8-3491h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2)
fir_c_addsub_v12_0_i32default:default2
232default:default2
12default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i3/synth/fir_c_addsub_v12_0_i3.vhd2default:default2
692default:default8@Z8-256h px� 
�
synthesizing module '%s'%s4497*oasys2/
align_input__parameterized42default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2602default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 35 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter abs_delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2.
extend_msb__parameterized72default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 35 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2,
sign_ext__parameterized72default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 35 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2,
sign_ext__parameterized72default:default2
 2default:default2
232default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2.
extend_msb__parameterized72default:default2
 2default:default2
232default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2/
align_input__parameterized42default:default2
 2default:default2
232default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2602default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2/
align_input__parameterized52default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2602default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 35 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter abs_delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2.
extend_msb__parameterized82default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 35 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2,
sign_ext__parameterized82default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 34 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 35 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2,
sign_ext__parameterized82default:default2
 2default:default2
232default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2.
extend_msb__parameterized82default:default2
 2default:default2
232default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2/
align_input__parameterized52default:default2
 2default:default2
232default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2602default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys20
convert_type__parameterized22default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7752default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 35 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 35 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter quantization bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter overflow bound to: 1 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter fp_width bound to: 32'sb00000000000000000000000000100101 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter fp_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter fp_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter q_width bound to: 32'sb00000000000000000000000000100101 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter q_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter q_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2)
trunc__parameterized22default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
5392default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100101 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000100101 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
x
%s
*synth2`
L	Parameter abs_right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2.
extend_msb__parameterized92default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100101 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000100101 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2,
sign_ext__parameterized92default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100101 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000100101 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2,
sign_ext__parameterized92default:default2
 2default:default2
232default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2.
extend_msb__parameterized92default:default2
 2default:default2
232default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2)
trunc__parameterized22default:default2
 2default:default2
232default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
5392default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2.
wrap_arith__parameterized22default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7542default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100101 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 35 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter result_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2(
cast__parameterized52default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
132default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100101 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 35 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2(
cast__parameterized52default:default2
 2default:default2
232default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
132default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2.
wrap_arith__parameterized22default:default2
 2default:default2
232default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7542default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2(
cast__parameterized62default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
132default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 35 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000100101 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2(
cast__parameterized62default:default2
 2default:default2
232default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
132default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys20
convert_type__parameterized22default:default2
 2default:default2
232default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7752default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys20
fir_xladdsub__parameterized22default:default2
 2default:default2
232default:default2
12default:default2�
g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir_entity_declarations.v2default:default2
1242default:default8@Z8-6155h px� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
addsub22default:default2 
fir_xladdsub2default:default2
102default:default2
72default:default2�
kg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir.v2default:default2
1792default:default8@Z8-350h px� 
�
synthesizing module '%s'%s4497*oasys20
fir_xladdsub__parameterized32default:default2
 2default:default2�
g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir_entity_declarations.v2default:default2
1242default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter core_name0 bound to: fir_c_addsub_v12_0_i4 - type: string 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter a_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter a_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter a_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_in_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_in_bin_pt bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_in_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter c_out_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter c_out_bin_pt bound to: 4 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_out_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter b_width bound to: 35 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter b_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter b_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter s_width bound to: 36 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter s_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter s_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter rst_width bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter rst_bin_pt bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter rst_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter en_width bound to: 1 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter en_bin_pt bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter en_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_s_width bound to: 36 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter full_s_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter mode bound to: 1 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter extra_registers bound to: 0 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter quantization bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter overflow bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_a_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter c_b_width bound to: 8 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_a_type bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_b_type bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_has_sclr bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_has_ce bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter c_latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter c_output_width bound to: 36 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter c_enable_rlocs bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_has_c_in bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_has_c_out bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_a_width bound to: 36 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_b_width bound to: 36 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter full_s_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2)
fir_c_addsub_v12_0_i42default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i4/synth/fir_c_addsub_v12_0_i4.vhd2default:default2
692default:default8@Z8-638h px� 
`
%s
*synth2H
4	Parameter C_VERBOSITY bound to: 0 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_XDEVICEFAMILY bound to: kintex7 - type: string 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_IMPLEMENTATION bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_A_WIDTH bound to: 36 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_B_WIDTH bound to: 36 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_OUT_WIDTH bound to: 36 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_CE_OVERRIDES_SCLR bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_A_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_B_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter C_LATENCY bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_ADD_MODE bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_B_CONSTANT bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
%s
*synth2h
T	Parameter C_B_VALUE bound to: 000000000000000000000000000000000000 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_AINIT_VAL bound to: 0 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_SINIT_VAL bound to: 0 - type: string 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_CE_OVERRIDES_BYPASS bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_BYPASS_LOW bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_SCLR_OVERRIDES_SSET bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_C_IN bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_HAS_C_OUT bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_BORROW_LOW bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_HAS_CE bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_HAS_BYPASS bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_SCLR bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_SSET bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_HAS_SINIT bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2%
c_addsub_v12_0_122default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i0/hdl/c_addsub_v12_0_vh_rfs.vhd2default:default2
70262default:default2
U02default:default2%
c_addsub_v12_0_122default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i4/synth/fir_c_addsub_v12_0_i4.vhd2default:default2
1362default:default8@Z8-3491h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2)
fir_c_addsub_v12_0_i42default:default2
242default:default2
12default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i4/synth/fir_c_addsub_v12_0_i4.vhd2default:default2
692default:default8@Z8-256h px� 
�
synthesizing module '%s'%s4497*oasys2/
align_input__parameterized62default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2602default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 36 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter abs_delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2/
extend_msb__parameterized102default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 36 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2-
sign_ext__parameterized102default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 36 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2-
sign_ext__parameterized102default:default2
 2default:default2
242default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2/
extend_msb__parameterized102default:default2
 2default:default2
242default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2/
align_input__parameterized62default:default2
 2default:default2
242default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2602default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2/
align_input__parameterized72default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2602default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 35 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 36 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter abs_delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2/
extend_msb__parameterized112default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 35 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 36 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2-
sign_ext__parameterized112default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 35 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 36 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2-
sign_ext__parameterized112default:default2
 2default:default2
242default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2/
extend_msb__parameterized112default:default2
 2default:default2
242default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2/
align_input__parameterized72default:default2
 2default:default2
242default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2602default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys20
convert_type__parameterized32default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7752default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 36 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 36 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter quantization bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter overflow bound to: 1 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter fp_width bound to: 32'sb00000000000000000000000000100110 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter fp_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter fp_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter q_width bound to: 32'sb00000000000000000000000000100110 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter q_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter q_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2)
trunc__parameterized32default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
5392default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100110 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000100110 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
x
%s
*synth2`
L	Parameter abs_right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2/
extend_msb__parameterized122default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100110 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000100110 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2-
sign_ext__parameterized122default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100110 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000100110 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2-
sign_ext__parameterized122default:default2
 2default:default2
242default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2/
extend_msb__parameterized122default:default2
 2default:default2
242default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2)
trunc__parameterized32default:default2
 2default:default2
242default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
5392default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2.
wrap_arith__parameterized32default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7542default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100110 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 36 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter result_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2(
cast__parameterized72default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
132default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100110 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 36 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2(
cast__parameterized72default:default2
 2default:default2
242default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
132default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2.
wrap_arith__parameterized32default:default2
 2default:default2
242default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7542default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2(
cast__parameterized82default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
132default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 36 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000100110 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2(
cast__parameterized82default:default2
 2default:default2
242default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
132default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys20
convert_type__parameterized32default:default2
 2default:default2
242default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7752default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys20
fir_xladdsub__parameterized32default:default2
 2default:default2
242default:default2
12default:default2�
g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir_entity_declarations.v2default:default2
1242default:default8@Z8-6155h px� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
addsub32default:default2 
fir_xladdsub2default:default2
102default:default2
72default:default2�
kg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir.v2default:default2
2092default:default8@Z8-350h px� 
�
synthesizing module '%s'%s4497*oasys20
fir_xladdsub__parameterized42default:default2
 2default:default2�
g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir_entity_declarations.v2default:default2
1242default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter core_name0 bound to: fir_c_addsub_v12_0_i5 - type: string 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter a_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter a_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter a_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_in_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_in_bin_pt bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_in_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter c_out_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter c_out_bin_pt bound to: 4 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_out_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter b_width bound to: 36 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter b_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter b_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter s_width bound to: 37 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter s_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter s_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter rst_width bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter rst_bin_pt bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter rst_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter en_width bound to: 1 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter en_bin_pt bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter en_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_s_width bound to: 37 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter full_s_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter mode bound to: 1 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter extra_registers bound to: 0 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter quantization bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter overflow bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_a_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter c_b_width bound to: 8 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_a_type bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_b_type bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_has_sclr bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_has_ce bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter c_latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter c_output_width bound to: 37 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter c_enable_rlocs bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_has_c_in bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_has_c_out bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_a_width bound to: 37 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_b_width bound to: 37 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter full_s_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2)
fir_c_addsub_v12_0_i52default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i5/synth/fir_c_addsub_v12_0_i5.vhd2default:default2
692default:default8@Z8-638h px� 
`
%s
*synth2H
4	Parameter C_VERBOSITY bound to: 0 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_XDEVICEFAMILY bound to: kintex7 - type: string 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_IMPLEMENTATION bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_A_WIDTH bound to: 37 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_B_WIDTH bound to: 37 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_OUT_WIDTH bound to: 37 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_CE_OVERRIDES_SCLR bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_A_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_B_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter C_LATENCY bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_ADD_MODE bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_B_CONSTANT bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
%s
*synth2i
U	Parameter C_B_VALUE bound to: 0000000000000000000000000000000000000 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_AINIT_VAL bound to: 0 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_SINIT_VAL bound to: 0 - type: string 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_CE_OVERRIDES_BYPASS bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_BYPASS_LOW bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_SCLR_OVERRIDES_SSET bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_C_IN bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_HAS_C_OUT bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_BORROW_LOW bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_HAS_CE bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_HAS_BYPASS bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_SCLR bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_SSET bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_HAS_SINIT bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2%
c_addsub_v12_0_122default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i0/hdl/c_addsub_v12_0_vh_rfs.vhd2default:default2
70262default:default2
U02default:default2%
c_addsub_v12_0_122default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i5/synth/fir_c_addsub_v12_0_i5.vhd2default:default2
1362default:default8@Z8-3491h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2)
fir_c_addsub_v12_0_i52default:default2
252default:default2
12default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i5/synth/fir_c_addsub_v12_0_i5.vhd2default:default2
692default:default8@Z8-256h px� 
�
synthesizing module '%s'%s4497*oasys2/
align_input__parameterized82default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2602default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 37 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter abs_delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2/
extend_msb__parameterized132default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 37 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2-
sign_ext__parameterized132default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 37 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2-
sign_ext__parameterized132default:default2
 2default:default2
252default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2/
extend_msb__parameterized132default:default2
 2default:default2
252default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2/
align_input__parameterized82default:default2
 2default:default2
252default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2602default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2/
align_input__parameterized92default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2602default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 36 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 37 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter abs_delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2/
extend_msb__parameterized142default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 36 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 37 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2-
sign_ext__parameterized142default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 36 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 37 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2-
sign_ext__parameterized142default:default2
 2default:default2
252default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2/
extend_msb__parameterized142default:default2
 2default:default2
252default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2/
align_input__parameterized92default:default2
 2default:default2
252default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2602default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys20
convert_type__parameterized42default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7752default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 37 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 37 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter quantization bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter overflow bound to: 1 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter fp_width bound to: 32'sb00000000000000000000000000100111 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter fp_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter fp_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter q_width bound to: 32'sb00000000000000000000000000100111 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter q_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter q_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2)
trunc__parameterized42default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
5392default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100111 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000100111 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
x
%s
*synth2`
L	Parameter abs_right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2/
extend_msb__parameterized152default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100111 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000100111 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2-
sign_ext__parameterized152default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100111 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000100111 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2-
sign_ext__parameterized152default:default2
 2default:default2
252default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2/
extend_msb__parameterized152default:default2
 2default:default2
252default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2)
trunc__parameterized42default:default2
 2default:default2
252default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
5392default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2.
wrap_arith__parameterized42default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7542default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100111 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 37 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter result_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2(
cast__parameterized92default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
132default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100111 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 37 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2(
cast__parameterized92default:default2
 2default:default2
252default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
132default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2.
wrap_arith__parameterized42default:default2
 2default:default2
252default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7542default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2)
cast__parameterized102default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
132default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 37 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000100111 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2)
cast__parameterized102default:default2
 2default:default2
252default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
132default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys20
convert_type__parameterized42default:default2
 2default:default2
252default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7752default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys20
fir_xladdsub__parameterized42default:default2
 2default:default2
252default:default2
12default:default2�
g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir_entity_declarations.v2default:default2
1242default:default8@Z8-6155h px� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
addsub42default:default2 
fir_xladdsub2default:default2
102default:default2
72default:default2�
kg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir.v2default:default2
2392default:default8@Z8-350h px� 
�
synthesizing module '%s'%s4497*oasys20
fir_xladdsub__parameterized52default:default2
 2default:default2�
g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir_entity_declarations.v2default:default2
1242default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter core_name0 bound to: fir_c_addsub_v12_0_i6 - type: string 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter a_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter a_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter a_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_in_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_in_bin_pt bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_in_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter c_out_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter c_out_bin_pt bound to: 4 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_out_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter b_width bound to: 37 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter b_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter b_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter s_width bound to: 38 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter s_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter s_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter rst_width bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter rst_bin_pt bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter rst_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter en_width bound to: 1 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter en_bin_pt bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter en_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_s_width bound to: 38 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter full_s_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter mode bound to: 1 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter extra_registers bound to: 0 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter quantization bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter overflow bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_a_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter c_b_width bound to: 8 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_a_type bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_b_type bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_has_sclr bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_has_ce bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter c_latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter c_output_width bound to: 38 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter c_enable_rlocs bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_has_c_in bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_has_c_out bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_a_width bound to: 38 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_b_width bound to: 38 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter full_s_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2)
fir_c_addsub_v12_0_i62default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i6/synth/fir_c_addsub_v12_0_i6.vhd2default:default2
692default:default8@Z8-638h px� 
`
%s
*synth2H
4	Parameter C_VERBOSITY bound to: 0 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_XDEVICEFAMILY bound to: kintex7 - type: string 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_IMPLEMENTATION bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_A_WIDTH bound to: 38 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_B_WIDTH bound to: 38 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_OUT_WIDTH bound to: 38 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_CE_OVERRIDES_SCLR bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_A_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_B_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter C_LATENCY bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_ADD_MODE bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_B_CONSTANT bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
%s
*synth2j
V	Parameter C_B_VALUE bound to: 00000000000000000000000000000000000000 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_AINIT_VAL bound to: 0 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_SINIT_VAL bound to: 0 - type: string 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_CE_OVERRIDES_BYPASS bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_BYPASS_LOW bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_SCLR_OVERRIDES_SSET bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_C_IN bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_HAS_C_OUT bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_BORROW_LOW bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_HAS_CE bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_HAS_BYPASS bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_SCLR bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_SSET bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_HAS_SINIT bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2%
c_addsub_v12_0_122default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i0/hdl/c_addsub_v12_0_vh_rfs.vhd2default:default2
70262default:default2
U02default:default2%
c_addsub_v12_0_122default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i6/synth/fir_c_addsub_v12_0_i6.vhd2default:default2
1362default:default8@Z8-3491h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2)
fir_c_addsub_v12_0_i62default:default2
262default:default2
12default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i6/synth/fir_c_addsub_v12_0_i6.vhd2default:default2
692default:default8@Z8-256h px� 
�
synthesizing module '%s'%s4497*oasys20
align_input__parameterized102default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2602default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 38 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter abs_delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2/
extend_msb__parameterized162default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 38 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2-
sign_ext__parameterized162default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 38 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2-
sign_ext__parameterized162default:default2
 2default:default2
262default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2/
extend_msb__parameterized162default:default2
 2default:default2
262default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys20
align_input__parameterized102default:default2
 2default:default2
262default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2602default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys20
align_input__parameterized112default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2602default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 37 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 38 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter abs_delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2/
extend_msb__parameterized172default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 37 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 38 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2-
sign_ext__parameterized172default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 37 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 38 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2-
sign_ext__parameterized172default:default2
 2default:default2
262default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2/
extend_msb__parameterized172default:default2
 2default:default2
262default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys20
align_input__parameterized112default:default2
 2default:default2
262default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2602default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys20
convert_type__parameterized52default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7752default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 38 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 38 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter quantization bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter overflow bound to: 1 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter fp_width bound to: 32'sb00000000000000000000000000101000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter fp_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter fp_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter q_width bound to: 32'sb00000000000000000000000000101000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter q_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter q_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2)
trunc__parameterized52default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
5392default:default8@Z8-6157h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101000 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000101000 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
x
%s
*synth2`
L	Parameter abs_right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2/
extend_msb__parameterized182default:default2
 2default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6157h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-61572default:default2
1002default:defaultZ17-14h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101000 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000101000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101000 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000101000 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2-
sign_ext__parameterized182default:default2
 2default:default2
262default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2112default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2/
extend_msb__parameterized182default:default2
 2default:default2
262default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
2352default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2)
trunc__parameterized52default:default2
 2default:default2
262default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
5392default:default8@Z8-6155h px� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101000 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 38 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter result_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101000 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 38 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2)
cast__parameterized112default:default2
 2default:default2
262default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
132default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2.
wrap_arith__parameterized52default:default2
 2default:default2
262default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7542default:default8@Z8-6155h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 38 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000101000 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2)
cast__parameterized122default:default2
 2default:default2
262default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
132default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys20
convert_type__parameterized52default:default2
 2default:default2
262default:default2
12default:default2�
tg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/convert_type.v2default:default2
7752default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys20
fir_xladdsub__parameterized52default:default2
 2default:default2
262default:default2
12default:default2�
g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir_entity_declarations.v2default:default2
1242default:default8@Z8-6155h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-61552default:default2
1002default:defaultZ17-14h px� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
addsub52default:default2 
fir_xladdsub2default:default2
102default:default2
72default:default2�
kg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir.v2default:default2
2692default:default8@Z8-350h px� 
r
%s
*synth2Z
F	Parameter core_name0 bound to: fir_c_addsub_v12_0_i7 - type: string 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter a_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter a_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter a_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_in_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_in_bin_pt bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_in_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter c_out_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter c_out_bin_pt bound to: 4 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_out_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter b_width bound to: 38 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter b_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter b_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter s_width bound to: 39 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter s_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter s_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter rst_width bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter rst_bin_pt bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter rst_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter en_width bound to: 1 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter en_bin_pt bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter en_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_s_width bound to: 39 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter full_s_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter mode bound to: 1 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter extra_registers bound to: 0 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter quantization bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter overflow bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_a_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter c_b_width bound to: 8 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_a_type bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_b_type bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_has_sclr bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_has_ce bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter c_latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter c_output_width bound to: 39 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter c_enable_rlocs bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_has_c_in bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_has_c_out bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_a_width bound to: 39 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_b_width bound to: 39 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter full_s_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2)
fir_c_addsub_v12_0_i72default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i7/synth/fir_c_addsub_v12_0_i7.vhd2default:default2
692default:default8@Z8-638h px� 
`
%s
*synth2H
4	Parameter C_VERBOSITY bound to: 0 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_XDEVICEFAMILY bound to: kintex7 - type: string 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_IMPLEMENTATION bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_A_WIDTH bound to: 39 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_B_WIDTH bound to: 39 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_OUT_WIDTH bound to: 39 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_CE_OVERRIDES_SCLR bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_A_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_B_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter C_LATENCY bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_ADD_MODE bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_B_CONSTANT bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
%s
*synth2k
W	Parameter C_B_VALUE bound to: 000000000000000000000000000000000000000 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_AINIT_VAL bound to: 0 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_SINIT_VAL bound to: 0 - type: string 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_CE_OVERRIDES_BYPASS bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_BYPASS_LOW bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_SCLR_OVERRIDES_SSET bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_C_IN bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_HAS_C_OUT bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_BORROW_LOW bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_HAS_CE bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_HAS_BYPASS bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_SCLR bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_SSET bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_HAS_SINIT bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2%
c_addsub_v12_0_122default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i0/hdl/c_addsub_v12_0_vh_rfs.vhd2default:default2
70262default:default2
U02default:default2%
c_addsub_v12_0_122default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i7/synth/fir_c_addsub_v12_0_i7.vhd2default:default2
1362default:default8@Z8-3491h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2)
fir_c_addsub_v12_0_i72default:default2
272default:default2
12default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i7/synth/fir_c_addsub_v12_0_i7.vhd2default:default2
692default:default8@Z8-256h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 39 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter abs_delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 39 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 39 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 38 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 39 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter abs_delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 38 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 39 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 38 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 39 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 39 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 39 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter quantization bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter overflow bound to: 1 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter fp_width bound to: 32'sb00000000000000000000000000101001 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter fp_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter fp_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter q_width bound to: 32'sb00000000000000000000000000101001 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter q_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter q_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101001 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000101001 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
x
%s
*synth2`
L	Parameter abs_right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101001 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000101001 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101001 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000101001 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101001 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 39 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter result_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101001 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 39 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 39 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000101001 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
addsub62default:default2 
fir_xladdsub2default:default2
102default:default2
72default:default2�
kg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir.v2default:default2
2992default:default8@Z8-350h px� 
r
%s
*synth2Z
F	Parameter core_name0 bound to: fir_c_addsub_v12_0_i8 - type: string 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter a_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter a_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter a_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_in_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_in_bin_pt bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_in_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter c_out_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter c_out_bin_pt bound to: 4 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_out_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter b_width bound to: 39 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter b_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter b_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter s_width bound to: 40 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter s_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter s_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter rst_width bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter rst_bin_pt bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter rst_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter en_width bound to: 1 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter en_bin_pt bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter en_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_s_width bound to: 40 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter full_s_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter mode bound to: 1 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter extra_registers bound to: 0 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter quantization bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter overflow bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_a_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter c_b_width bound to: 8 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_a_type bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_b_type bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_has_sclr bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_has_ce bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter c_latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter c_output_width bound to: 40 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter c_enable_rlocs bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_has_c_in bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_has_c_out bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_a_width bound to: 40 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_b_width bound to: 40 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter full_s_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2)
fir_c_addsub_v12_0_i82default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i8/synth/fir_c_addsub_v12_0_i8.vhd2default:default2
692default:default8@Z8-638h px� 
`
%s
*synth2H
4	Parameter C_VERBOSITY bound to: 0 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_XDEVICEFAMILY bound to: kintex7 - type: string 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_IMPLEMENTATION bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_A_WIDTH bound to: 40 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_B_WIDTH bound to: 40 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_OUT_WIDTH bound to: 40 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_CE_OVERRIDES_SCLR bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_A_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_B_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter C_LATENCY bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_ADD_MODE bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_B_CONSTANT bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
%s
*synth2l
X	Parameter C_B_VALUE bound to: 0000000000000000000000000000000000000000 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_AINIT_VAL bound to: 0 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_SINIT_VAL bound to: 0 - type: string 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_CE_OVERRIDES_BYPASS bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_BYPASS_LOW bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_SCLR_OVERRIDES_SSET bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_C_IN bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_HAS_C_OUT bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_BORROW_LOW bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_HAS_CE bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_HAS_BYPASS bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_SCLR bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_SSET bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_HAS_SINIT bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2%
c_addsub_v12_0_122default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i0/hdl/c_addsub_v12_0_vh_rfs.vhd2default:default2
70262default:default2
U02default:default2%
c_addsub_v12_0_122default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i8/synth/fir_c_addsub_v12_0_i8.vhd2default:default2
1362default:default8@Z8-3491h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2)
fir_c_addsub_v12_0_i82default:default2
282default:default2
12default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i8/synth/fir_c_addsub_v12_0_i8.vhd2default:default2
692default:default8@Z8-256h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 40 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter abs_delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 40 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 40 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 39 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 40 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter abs_delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 39 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 40 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 39 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 40 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 40 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 40 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter quantization bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter overflow bound to: 1 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter fp_width bound to: 32'sb00000000000000000000000000101010 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter fp_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter fp_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter q_width bound to: 32'sb00000000000000000000000000101010 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter q_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter q_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101010 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000101010 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
x
%s
*synth2`
L	Parameter abs_right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101010 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000101010 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101010 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000101010 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101010 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 40 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter result_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101010 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 40 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 40 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000101010 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
addsub72default:default2 
fir_xladdsub2default:default2
102default:default2
72default:default2�
kg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir.v2default:default2
3292default:default8@Z8-350h px� 
r
%s
*synth2Z
F	Parameter core_name0 bound to: fir_c_addsub_v12_0_i9 - type: string 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter a_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter a_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter a_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_in_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_in_bin_pt bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_in_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter c_out_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter c_out_bin_pt bound to: 4 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_out_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter b_width bound to: 40 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter b_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter b_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter s_width bound to: 41 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter s_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter s_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter rst_width bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter rst_bin_pt bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter rst_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter en_width bound to: 1 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter en_bin_pt bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter en_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_s_width bound to: 41 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter full_s_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter mode bound to: 1 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter extra_registers bound to: 0 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter quantization bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter overflow bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_a_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter c_b_width bound to: 8 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_a_type bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_b_type bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_has_sclr bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_has_ce bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter c_latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter c_output_width bound to: 41 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter c_enable_rlocs bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_has_c_in bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_has_c_out bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_a_width bound to: 41 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_b_width bound to: 41 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter full_s_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2)
fir_c_addsub_v12_0_i92default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i9/synth/fir_c_addsub_v12_0_i9.vhd2default:default2
692default:default8@Z8-638h px� 
`
%s
*synth2H
4	Parameter C_VERBOSITY bound to: 0 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_XDEVICEFAMILY bound to: kintex7 - type: string 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_IMPLEMENTATION bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_A_WIDTH bound to: 41 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_B_WIDTH bound to: 41 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_OUT_WIDTH bound to: 41 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_CE_OVERRIDES_SCLR bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_A_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_B_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter C_LATENCY bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_ADD_MODE bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_B_CONSTANT bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
%s
*synth2m
Y	Parameter C_B_VALUE bound to: 00000000000000000000000000000000000000000 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_AINIT_VAL bound to: 0 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_SINIT_VAL bound to: 0 - type: string 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_CE_OVERRIDES_BYPASS bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_BYPASS_LOW bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_SCLR_OVERRIDES_SSET bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_C_IN bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_HAS_C_OUT bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_BORROW_LOW bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_HAS_CE bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_HAS_BYPASS bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_SCLR bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_SSET bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_HAS_SINIT bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2%
c_addsub_v12_0_122default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i0/hdl/c_addsub_v12_0_vh_rfs.vhd2default:default2
70262default:default2
U02default:default2%
c_addsub_v12_0_122default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i9/synth/fir_c_addsub_v12_0_i9.vhd2default:default2
1362default:default8@Z8-3491h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2)
fir_c_addsub_v12_0_i92default:default2
292default:default2
12default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i9/synth/fir_c_addsub_v12_0_i9.vhd2default:default2
692default:default8@Z8-256h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 41 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter abs_delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 41 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 41 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 40 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 41 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter abs_delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 40 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 41 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 40 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 41 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 41 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 41 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter quantization bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter overflow bound to: 1 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter fp_width bound to: 32'sb00000000000000000000000000101011 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter fp_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter fp_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter q_width bound to: 32'sb00000000000000000000000000101011 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter q_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter q_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101011 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000101011 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
x
%s
*synth2`
L	Parameter abs_right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101011 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000101011 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101011 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000101011 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101011 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 41 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter result_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101011 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 41 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 41 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000101011 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
addsub82default:default2 
fir_xladdsub2default:default2
102default:default2
72default:default2�
kg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir.v2default:default2
3592default:default8@Z8-350h px� 
s
%s
*synth2[
G	Parameter core_name0 bound to: fir_c_addsub_v12_0_i10 - type: string 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter a_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter a_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter a_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_in_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_in_bin_pt bound to: 4 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_in_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter c_out_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter c_out_bin_pt bound to: 4 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_out_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter b_width bound to: 41 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter b_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter b_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter s_width bound to: 42 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter s_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter s_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter rst_width bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter rst_bin_pt bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter rst_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter en_width bound to: 1 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter en_bin_pt bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter en_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_s_width bound to: 42 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter full_s_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter mode bound to: 1 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter extra_registers bound to: 0 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter quantization bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter overflow bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_a_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter c_b_width bound to: 8 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_a_type bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_b_type bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_has_sclr bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_has_ce bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter c_latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter c_output_width bound to: 42 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter c_enable_rlocs bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_has_c_in bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter c_has_c_out bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_a_width bound to: 42 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter full_b_width bound to: 42 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter full_s_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2*
fir_c_addsub_v12_0_i102default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i10/synth/fir_c_addsub_v12_0_i10.vhd2default:default2
692default:default8@Z8-638h px� 
`
%s
*synth2H
4	Parameter C_VERBOSITY bound to: 0 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_XDEVICEFAMILY bound to: kintex7 - type: string 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_IMPLEMENTATION bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_A_WIDTH bound to: 42 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_B_WIDTH bound to: 42 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_OUT_WIDTH bound to: 42 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_CE_OVERRIDES_SCLR bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_A_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_B_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter C_LATENCY bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_ADD_MODE bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_B_CONSTANT bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
%s
*synth2n
Z	Parameter C_B_VALUE bound to: 000000000000000000000000000000000000000000 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_AINIT_VAL bound to: 0 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_SINIT_VAL bound to: 0 - type: string 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_CE_OVERRIDES_BYPASS bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_BYPASS_LOW bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_SCLR_OVERRIDES_SSET bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_C_IN bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_HAS_C_OUT bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_BORROW_LOW bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_HAS_CE bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_HAS_BYPASS bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_SCLR bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_SSET bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_HAS_SINIT bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2%
c_addsub_v12_0_122default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i0/hdl/c_addsub_v12_0_vh_rfs.vhd2default:default2
70262default:default2
U02default:default2%
c_addsub_v12_0_122default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i10/synth/fir_c_addsub_v12_0_i10.vhd2default:default2
1362default:default8@Z8-3491h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2*
fir_c_addsub_v12_0_i102default:default2
302default:default2
12default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_c_addsub_v12_0_i10/synth/fir_c_addsub_v12_0_i10.vhd2default:default2
692default:default8@Z8-256h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 42 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter abs_delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 42 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 42 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 41 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 42 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter abs_delta bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 41 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 42 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 41 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 42 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 42 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 42 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter quantization bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter overflow bound to: 1 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter fp_width bound to: 32'sb00000000000000000000000000101100 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter fp_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter fp_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter q_width bound to: 32'sb00000000000000000000000000101100 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter q_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter q_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101100 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000101100 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
x
%s
*synth2`
L	Parameter abs_right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101100 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000101100 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101100 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000101100 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101100 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 42 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter result_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000101100 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 42 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 42 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000101100 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
addsub92default:default2 
fir_xladdsub2default:default2
102default:default2
72default:default2�
kg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/fir.v2default:default2
3892default:default8@Z8-350h px� 
[
%s
*synth2C
/	Parameter width bound to: 43 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter reg_retiming bound to: 0 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter reset bound to: 0 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter width bound to: 43 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter complete_num_srlc33es bound to: 0 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter remaining_latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter temp_num_srlc33es bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter num_srlc33es bound to: 1 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter width bound to: 43 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
O
%s
*synth27
#	Parameter a bound to: 6'sb111111 
2default:defaulth p
x
� 
L
%s
*synth24
 	Parameter INIT bound to: 1'b0 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter width bound to: 16 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter latency bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter reg_retiming bound to: 0 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter reset bound to: 0 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter width bound to: 16 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter latency bound to: 2 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter complete_num_srlc33es bound to: 0 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter remaining_latency bound to: 2 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter temp_num_srlc33es bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter num_srlc33es bound to: 1 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter width bound to: 16 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter latency bound to: 2 - type: integer 
2default:defaulth p
x
� 
O
%s
*synth27
#	Parameter a bound to: 6'sb000000 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter INIT bound to: 32'b00000000000000000000000000000000 
2default:defaulth p
x
� 
W
%s
*synth2?
+	Parameter IS_CLK_INVERTED bound to: 1'b0 
2default:defaulth p
x
� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
u12default:default2
SRLC32E2default:default2
62default:default2
52default:default2�
qg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/synth_reg.v2default:default2
762default:default8@Z8-350h px� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
u12default:default2
SRLC32E2default:default2
62default:default2
52default:default2�
qg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/synth_reg.v2default:default2
762default:default8@Z8-350h px� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
u12default:default2
SRLC32E2default:default2
62default:default2
52default:default2�
qg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/synth_reg.v2default:default2
762default:default8@Z8-350h px� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
u12default:default2
SRLC32E2default:default2
62default:default2
52default:default2�
qg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/synth_reg.v2default:default2
762default:default8@Z8-350h px� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
u12default:default2
SRLC32E2default:default2
62default:default2
52default:default2�
qg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/synth_reg.v2default:default2
762default:default8@Z8-350h px� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
u12default:default2
SRLC32E2default:default2
62default:default2
52default:default2�
qg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/synth_reg.v2default:default2
762default:default8@Z8-350h px� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
u12default:default2
SRLC32E2default:default2
62default:default2
52default:default2�
qg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/synth_reg.v2default:default2
762default:default8@Z8-350h px� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
u12default:default2
SRLC32E2default:default2
62default:default2
52default:default2�
qg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/synth_reg.v2default:default2
762default:default8@Z8-350h px� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
u12default:default2
SRLC32E2default:default2
62default:default2
52default:default2�
qg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/synth_reg.v2default:default2
762default:default8@Z8-350h px� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
u12default:default2
SRLC32E2default:default2
62default:default2
52default:default2�
qg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/synth_reg.v2default:default2
762default:default8@Z8-350h px� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
u12default:default2
SRLC32E2default:default2
62default:default2
52default:default2�
qg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/synth_reg.v2default:default2
762default:default8@Z8-350h px� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
u12default:default2
SRLC32E2default:default2
62default:default2
52default:default2�
qg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/synth_reg.v2default:default2
762default:default8@Z8-350h px� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
u12default:default2
SRLC32E2default:default2
62default:default2
52default:default2�
qg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/synth_reg.v2default:default2
762default:default8@Z8-350h px� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
u12default:default2
SRLC32E2default:default2
62default:default2
52default:default2�
qg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/synth_reg.v2default:default2
762default:default8@Z8-350h px� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
u12default:default2
SRLC32E2default:default2
62default:default2
52default:default2�
qg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/synth_reg.v2default:default2
762default:default8@Z8-350h px� 
�
Ginstance '%s' of module '%s' requires %s connections, but only %s given350*oasys2
u12default:default2
SRLC32E2default:default2
62default:default2
52default:default2�
qg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/work/hdl/synth_reg.v2default:default2
762default:default8@Z8-350h px� 
[
%s
*synth2C
/	Parameter width bound to: 16 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter reg_retiming bound to: 0 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter reset bound to: 0 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter width bound to: 16 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter complete_num_srlc33es bound to: 0 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter remaining_latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter temp_num_srlc33es bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter num_srlc33es bound to: 1 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter width bound to: 16 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter latency bound to: 1 - type: integer 
2default:defaulth p
x
� 
O
%s
*synth27
#	Parameter a bound to: 6'sb111111 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter core_name0 bound to: fir_mult_gen_v12_0_i0 - type: string 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter a_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter a_bin_pt bound to: 14 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter a_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter b_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter b_bin_pt bound to: 14 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter b_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter p_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter p_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter p_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter rst_width bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter rst_bin_pt bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter rst_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter en_width bound to: 1 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter en_bin_pt bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter en_arith bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter quantization bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter overflow bound to: 1 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter extra_registers bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_a_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter c_b_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter c_type bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_a_type bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter c_b_type bound to: 0 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter c_baat bound to: 16 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter oversample bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter multsign bound to: 2 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter c_output_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2)
fir_mult_gen_v12_0_i02default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_mult_gen_v12_0_i0/synth/fir_mult_gen_v12_0_i0.vhd2default:default2
702default:default8@Z8-638h px� 
�
-Port '%s' is missing in component declaration4102*oasys2
zero_detect2default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_mult_gen_v12_0_i0/synth/fir_mult_gen_v12_0_i0.vhd2default:default2
732default:default8@Z8-5640h px� 
�
-Port '%s' is missing in component declaration4102*oasys2
pcasc2default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_mult_gen_v12_0_i0/synth/fir_mult_gen_v12_0_i0.vhd2default:default2
732default:default8@Z8-5640h px� 
`
%s
*synth2H
4	Parameter C_VERBOSITY bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_MODEL_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter C_OPTIMIZE_GOAL bound to: 1 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_XDEVICEFAMILY bound to: kintex7 - type: string 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_HAS_CE bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_HAS_SCLR bound to: 1 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter C_LATENCY bound to: 3 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_A_WIDTH bound to: 16 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_A_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_B_WIDTH bound to: 16 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_B_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_OUT_HIGH bound to: 31 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter C_OUT_LOW bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_MULT_TYPE bound to: 1 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_CE_OVERRIDES_SCLR bound to: 1 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter C_CCM_IMP bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter C_B_VALUE bound to: 10000001 - type: string 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter C_HAS_ZERO_DETECT bound to: 0 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_ROUND_OUTPUT bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_ROUND_PT bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2%
mult_gen_v12_0_142default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_mult_gen_v12_0_i0/hdl/mult_gen_v12_0_vh_rfs.vhd2default:default2
210132default:default2
U02default:default2%
mult_gen_v12_0_142default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_mult_gen_v12_0_i0/synth/fir_mult_gen_v12_0_i0.vhd2default:default2
1262default:default8@Z8-3491h px� 
j
Hattribute "use_dsp48" has been deprecated, please use "use_dsp" instead 4323*oasysZ8-5974h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2)
fir_mult_gen_v12_0_i02default:default2
412default:default2
12default:default2�
�g:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/fir_mult_gen_v12_0_i0/synth/fir_mult_gen_v12_0_i0.vhd2default:default2
702default:default8@Z8-256h px� 
_
%s
*synth2G
3	Parameter old_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 16 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter old_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter quantization bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter overflow bound to: 1 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter fp_width bound to: 32'sb00000000000000000000000000100010 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter fp_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter fp_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter q_width bound to: 32'sb00000000000000000000000000100010 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter q_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter q_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100010 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter old_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000100010 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
x
%s
*synth2`
L	Parameter abs_right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100010 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000100010 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100010 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000100010 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100010 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter old_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter result_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter old_width bound to: 32'sb00000000000000000000000000100010 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter old_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter new_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter new_bin_pt bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter old_width bound to: 32 - type: integer 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter old_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter new_width bound to: 32'sb00000000000000000000000000100010 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter new_bin_pt bound to: 32'sb00000000000000000000000000011100 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter new_arith bound to: 2 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter right_of_dp bound to: 32'sb00000000000000000000000000000000 
2default:defaulth p
x
� 
�
!design %s has unconnected port %s3331*oasys2.
sysgen_constant_6ba6280cb72default:default2
clk2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2.
sysgen_constant_6ba6280cb72default:default2
ce2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2.
sysgen_constant_6ba6280cb72default:default2
clr2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2.
sysgen_constant_929f6abb9c2default:default2
clk2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2.
sysgen_constant_929f6abb9c2default:default2
ce2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2.
sysgen_constant_929f6abb9c2default:default2
clr2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2.
sysgen_constant_5976f291082default:default2
clk2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2.
sysgen_constant_5976f291082default:default2
ce2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2.
sysgen_constant_5976f291082default:default2
clr2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2.
sysgen_constant_23f796b59a2default:default2
clk2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2.
sysgen_constant_23f796b59a2default:default2
ce2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2.
sysgen_constant_23f796b59a2default:default2
clr2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2.
sysgen_constant_3093588e1d2default:default2
clk2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2.
sysgen_constant_3093588e1d2default:default2
ce2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2.
sysgen_constant_3093588e1d2default:default2
clr2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2.
sysgen_constant_047e8383512default:default2
clk2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2.
sysgen_constant_047e8383512default:default2
ce2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2.
sysgen_constant_047e8383512default:default2
clr2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2.
sysgen_constant_f1e53d55a12default:default2
clk2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2.
sysgen_constant_f1e53d55a12default:default2
ce2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2.
sysgen_constant_f1e53d55a12default:default2
clr2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2)
cast__parameterized212default:default2
inp[33]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2)
cast__parameterized212default:default2
inp[32]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2

delay_line2default:default2
CLK2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2

delay_line2default:default2
CE2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2

delay_line2default:default2
SCLR2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[47]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[46]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[45]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[44]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[43]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[42]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[41]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[40]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[39]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[38]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[37]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[36]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[35]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[34]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[33]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[32]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[31]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[30]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[29]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[28]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[27]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[26]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[25]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[24]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[23]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[22]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[21]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[20]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[19]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[18]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[17]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[16]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[15]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[14]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[13]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[12]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[11]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

C_PORT[10]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
dsp2default:default2
	C_PORT[9]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
dsp2default:default2
	C_PORT[8]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
dsp2default:default2
	C_PORT[7]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
dsp2default:default2
	C_PORT[6]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
dsp2default:default2
	C_PORT[5]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
dsp2default:default2
	C_PORT[4]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
dsp2default:default2
	C_PORT[3]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
dsp2default:default2
	C_PORT[2]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
dsp2default:default2
	C_PORT[1]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
dsp2default:default2
	C_PORT[0]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

D_PORT[24]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

D_PORT[23]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

D_PORT[22]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

D_PORT[21]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

D_PORT[20]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

D_PORT[19]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

D_PORT[18]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

D_PORT[17]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

D_PORT[16]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

D_PORT[15]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

D_PORT[14]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

D_PORT[13]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

D_PORT[12]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

D_PORT[11]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
dsp2default:default2

D_PORT[10]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
dsp2default:default2
	D_PORT[9]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
dsp2default:default2
	D_PORT[8]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
dsp2default:default2
	D_PORT[7]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
dsp2default:default2
	D_PORT[6]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
dsp2default:default2
	D_PORT[5]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
dsp2default:default2
	D_PORT[4]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
dsp2default:default2
	D_PORT[3]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
dsp2default:default2
	D_PORT[2]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
dsp2default:default2
	D_PORT[1]2default:defaultZ8-3331h px� 
{
!design %s has unconnected port %s3331*oasys2
dsp2default:default2
	D_PORT[0]2default:defaultZ8-3331h px� 
z
!design %s has unconnected port %s3331*oasys2
dsp2default:default2
SUBTRACT2default:defaultZ8-3331h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-33312default:default2
1002default:defaultZ17-14h px� 
�
%s*synth2�
xFinished RTL Elaboration : Time (s): cpu = 00:00:33 ; elapsed = 00:00:56 . Memory (MB): peak = 648.992 ; gain = 272.203
2default:defaulth px� 
D
%s
*synth2,

Report Check Netlist: 
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
M
%s
*synth25
!Start Handling Custom Attributes
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Handling Custom Attributes : Time (s): cpu = 00:00:33 ; elapsed = 00:00:57 . Memory (MB): peak = 648.992 ; gain = 272.203
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished RTL Optimization Phase 1 : Time (s): cpu = 00:00:33 ; elapsed = 00:00:57 . Memory (MB): peak = 648.992 ; gain = 272.203
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
h
-Analyzing %s Unisim elements for replacement
17*netlist2
10602default:defaultZ29-17h px� 
j
2Unisim Transformation completed in %s CPU seconds
28*netlist2
02default:defaultZ29-28h px� 
W
Loading part %s157*device2$
xc7k325tfbg676-32default:defaultZ21-403h px� 
K
)Preparing netlist for logic optimization
349*projectZ1-570h px� 
>

Processing XDC Constraints
244*projectZ1-262h px� 
=
Initializing timing engine
348*projectZ1-569h px� 
�
$Parsing XDC File [%s] for cell '%s'
848*designutils2�
lg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/constrs/fir.xdc2default:default2
inst	2default:default8Z20-848h px� 
�
-Finished Parsing XDC File [%s] for cell '%s'
847*designutils2�
lg:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.srcs/sources_1/ip/fir_0/constrs/fir.xdc2default:default2
inst	2default:default8Z20-847h px� 
�
Parsing XDC File [%s]
179*designutils2|
fG:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.runs/fir_0_synth_1/dont_touch.xdc2default:default8Z20-179h px� 
�
Finished Parsing XDC File [%s]
178*designutils2|
fG:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.runs/fir_0_synth_1/dont_touch.xdc2default:default8Z20-178h px� 
H
&Completed Processing XDC Constraints

245*projectZ1-263h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2 
00:00:00.0092default:default2
951.8242default:default2
0.0002default:defaultZ17-268h px� 
�
!Unisim Transformation Summary:
%s111*project2^
J  A total of 235 instances were transformed.
  FDE => FDRE: 235 instances
2default:defaultZ1-111h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2 
00:00:00.0052default:default2
951.8242default:default2
0.0002default:defaultZ17-268h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common24
 Constraint Validation Runtime : 2default:default2
00:00:002default:default2 
00:00:00.4082default:default2
952.5782default:default2
0.7542default:defaultZ17-268h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
~Finished Constraint Validation : Time (s): cpu = 00:00:49 ; elapsed = 00:01:31 . Memory (MB): peak = 952.578 ; gain = 575.789
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
V
%s
*synth2>
*Start Loading Part and Timing Information
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Loading part: xc7k325tfbg676-3
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Loading Part and Timing Information : Time (s): cpu = 00:00:49 ; elapsed = 00:01:32 . Memory (MB): peak = 952.578 ; gain = 575.789
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Z
%s
*synth2B
.Start Applying 'set_property' XDC Constraints
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished applying 'set_property' XDC Constraints : Time (s): cpu = 00:00:50 ; elapsed = 00:01:32 . Memory (MB): peak = 952.578 ; gain = 575.789
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished RTL Optimization Phase 2 : Time (s): cpu = 00:00:53 ; elapsed = 00:01:39 . Memory (MB): peak = 952.578 ; gain = 575.789
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
$decloning instance '%s' (%s) to '%s'223*oasys24
 inst/fir_struct/fir_x0/constant02default:default2.
sysgen_constant_047e8383512default:default25
!inst/fir_struct/fir_x0/constant112default:defaultZ8-223h px� 
�
$decloning instance '%s' (%s) to '%s'223*oasys24
 inst/fir_struct/fir_x0/constant12default:default2.
sysgen_constant_f1e53d55a12default:default25
!inst/fir_struct/fir_x0/constant102default:defaultZ8-223h px� 
�
$decloning instance '%s' (%s) to '%s'223*oasys24
 inst/fir_struct/fir_x0/constant22default:default2.
sysgen_constant_6ba6280cb72default:default24
 inst/fir_struct/fir_x0/constant92default:defaultZ8-223h px� 
�
$decloning instance '%s' (%s) to '%s'223*oasys24
 inst/fir_struct/fir_x0/constant32default:default2.
sysgen_constant_929f6abb9c2default:default24
 inst/fir_struct/fir_x0/constant82default:defaultZ8-223h px� 
�
$decloning instance '%s' (%s) to '%s'223*oasys24
 inst/fir_struct/fir_x0/constant42default:default2.
sysgen_constant_5976f291082default:default24
 inst/fir_struct/fir_x0/constant72default:defaultZ8-223h px� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Finished RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Y
%s
*synth2A
-Start RTL Hierarchical Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
[
%s
*synth2C
/Finished RTL Hierarchical Component Statistics
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2m
YPart Resources:
DSPs: 840 (col length:140)
BRAMs: 890 (col length: RAMB18 140 RAMB36 70)
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
W
%s
*synth2?
+Start Cross Boundary and Area Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
]
%s
*synth2E
1Warning: Parallel synthesis criteria is not met 
2default:defaulth p
x
� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2�
sfir_default_clock_driver/clockdriver/clr_reg/has_latency.fd_array[1].reg_comp_1/fd_prim_array[0].rst_comp.fdre_comp2default:default2
fir2default:defaultZ8-3332h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Cross Boundary and Area Optimization : Time (s): cpu = 00:00:56 ; elapsed = 00:01:43 . Memory (MB): peak = 952.578 ; gain = 575.789
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
R
%s
*synth2:
&Start Applying XDC Timing Constraints
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Applying XDC Timing Constraints : Time (s): cpu = 00:01:16 ; elapsed = 00:02:15 . Memory (MB): peak = 965.066 ; gain = 588.277
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
F
%s
*synth2.
Start Timing Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
|Finished Timing Optimization : Time (s): cpu = 00:01:16 ; elapsed = 00:02:15 . Memory (MB): peak = 965.438 ; gain = 588.648
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-
Start Technology Mapping
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
{Finished Technology Mapping : Time (s): cpu = 00:01:17 ; elapsed = 00:02:17 . Memory (MB): peak = 976.145 ; gain = 599.355
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
?
%s
*synth2'
Start IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Q
%s
*synth29
%Start Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
T
%s
*synth2<
(Finished Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
uFinished IO Insertion : Time (s): cpu = 00:01:19 ; elapsed = 00:02:21 . Memory (MB): peak = 976.145 ; gain = 599.355
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
D
%s
*synth2,

Report Check Netlist: 
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Start Renaming Generated Instances
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Instances : Time (s): cpu = 00:01:19 ; elapsed = 00:02:21 . Memory (MB): peak = 976.145 ; gain = 599.355
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start Rebuilding User Hierarchy
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Rebuilding User Hierarchy : Time (s): cpu = 00:01:19 ; elapsed = 00:02:21 . Memory (MB): peak = 976.145 ; gain = 599.355
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Renaming Generated Ports
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Ports : Time (s): cpu = 00:01:19 ; elapsed = 00:02:21 . Memory (MB): peak = 976.145 ; gain = 599.355
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
M
%s
*synth25
!Start Handling Custom Attributes
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Handling Custom Attributes : Time (s): cpu = 00:01:19 ; elapsed = 00:02:22 . Memory (MB): peak = 976.145 ; gain = 599.355
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
J
%s
*synth22
Start Renaming Generated Nets
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Nets : Time (s): cpu = 00:01:19 ; elapsed = 00:02:22 . Memory (MB): peak = 976.145 ; gain = 599.355
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Writing Synthesis Report
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
A
%s
*synth2)

Report BlackBoxes: 
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
J
%s
*synth22
| |BlackBox name |Instances |
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
A
%s*synth2)

Report Cell Usage: 
2default:defaulth px� 
E
%s*synth2-
+------+--------+------+
2default:defaulth px� 
E
%s*synth2-
|      |Cell    |Count |
2default:defaulth px� 
E
%s*synth2-
+------+--------+------+
2default:defaulth px� 
E
%s*synth2-
|1     |DSP48E1 |    12|
2default:defaulth px� 
E
%s*synth2-
|2     |LUT2    |   430|
2default:defaulth px� 
E
%s*synth2-
|3     |MUXCY   |   407|
2default:defaulth px� 
E
%s*synth2-
|4     |SRLC32E |   176|
2default:defaulth px� 
E
%s*synth2-
|5     |XORCY   |   418|
2default:defaulth px� 
E
%s*synth2-
|6     |FDE     |   235|
2default:defaulth px� 
E
%s*synth2-
|7     |FDRE    |   385|
2default:defaulth px� 
E
%s*synth2-
+------+--------+------+
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Writing Synthesis Report : Time (s): cpu = 00:01:19 ; elapsed = 00:02:22 . Memory (MB): peak = 976.145 ; gain = 599.355
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
s
%s
*synth2[
GSynthesis finished with 0 errors, 0 critical warnings and 69 warnings.
2default:defaulth p
x
� 
�
%s
*synth2�
~Synthesis Optimization Runtime : Time (s): cpu = 00:00:35 ; elapsed = 00:01:54 . Memory (MB): peak = 976.145 ; gain = 295.770
2default:defaulth p
x
� 
�
%s
*synth2�
Synthesis Optimization Complete : Time (s): cpu = 00:01:20 ; elapsed = 00:02:22 . Memory (MB): peak = 976.145 ; gain = 599.355
2default:defaulth p
x
� 
B
 Translating synthesized netlist
350*projectZ1-571h px� 
h
-Analyzing %s Unisim elements for replacement
17*netlist2
10722default:defaultZ29-17h px� 
j
2Unisim Transformation completed in %s CPU seconds
28*netlist2
02default:defaultZ29-28h px� 
K
)Preparing netlist for logic optimization
349*projectZ1-570h px� 
u
)Pushed %s inverter(s) to %s load pin(s).
98*opt2
02default:default2
02default:defaultZ31-138h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2 
00:00:00.0022default:default2
976.1452default:default2
0.0002default:defaultZ17-268h px� 
�
!Unisim Transformation Summary:
%s111*project2�
s  A total of 344 instances were transformed.
  (MUXCY,XORCY) => CARRY4: 109 instances
  FDE => FDRE: 235 instances
2default:defaultZ1-111h px� 
U
Releasing license: %s
83*common2
	Synthesis2default:defaultZ17-83h px� 
�
G%s Infos, %s Warnings, %s Critical Warnings and %s Errors encountered.
28*	vivadotcl2
2602default:default2
1342default:default2
02default:default2
02default:defaultZ4-41h px� 
^
%s completed successfully
29*	vivadotcl2 
synth_design2default:defaultZ4-42h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2"
synth_design: 2default:default2
00:01:262default:default2
00:02:362default:default2
976.1452default:default2
610.8242default:defaultZ17-268h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2 
00:00:00.0022default:default2
976.1452default:default2
0.0002default:defaultZ17-268h px� 
K
"No constraints selected for write.1103*constraintsZ18-5210h px� 
�
 The %s '%s' has been generated.
621*common2

checkpoint2default:default2u
aG:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.runs/fir_0_synth_1/fir_0.dcp2default:defaultZ17-1381h px� 
�
<Added synthesis output to IP cache for IP %s, cache-ID = %s
485*coretcl2
fir_02default:default2$
b7cb195474b88f742default:defaultZ2-1648h px� 
R
Renamed %s cell refs.
330*coretcl2
1892default:defaultZ2-1174h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2 
00:00:00.0032default:default2
976.1452default:default2
0.0002default:defaultZ17-268h px� 
K
"No constraints selected for write.1103*constraintsZ18-5210h px� 
�
 The %s '%s' has been generated.
621*common2

checkpoint2default:default2u
aG:/a_max/max_xilinx/max/System_Generator/FIR/FIR3/par/ip_catalog/fir.runs/fir_0_synth_1/fir_0.dcp2default:defaultZ17-1381h px� 
�
%s4*runtcl2t
`Executing : report_utilization -file fir_0_utilization_synth.rpt -pb fir_0_utilization_synth.pb
2default:defaulth px� 
�
Exiting %s at %s...
206*common2
Vivado2default:default2,
Wed Jul  8 22:22:06 20202default:defaultZ17-206h px� 


End Record