
�
Command: %s
53*	vivadotcl2p
\synth_design -top fir_filter_fir_compiler_v7_2_i0 -part xc7z020clg400-2 -mode out_of_context2default:defaultZ4-113h px� 
:
Starting synth_design
149*	vivadotclZ4-321h px� 
�
@Attempting to get a license for feature '%s' and/or device '%s'
308*common2
	Synthesis2default:default2
xc7z0202default:defaultZ17-347h px� 
�
0Got license for feature '%s' and/or device '%s'
310*common2
	Synthesis2default:default2
xc7z0202default:defaultZ17-349h px� 
�
%s*synth2�
wStarting RTL Elaboration : Time (s): cpu = 00:00:13 ; elapsed = 00:00:15 . Memory (MB): peak = 403.109 ; gain = 99.758
2default:defaulth px� 
�
synthesizing module '%s'638*oasys23
fir_filter_fir_compiler_v7_2_i02default:default2�
�e:/Bilibili_FPGA/N4_Project/VIVADO/hdl_netlist/fir_filter.srcs/sources_1/ip/fir_filter_fir_compiler_v7_2_i0/synth/fir_filter_fir_compiler_v7_2_i0.vhd2default:default2
712default:default8@Z8-638h px� 
f
%s
*synth2N
:	Parameter C_XDEVICEFAMILY bound to: zynq - type: string 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter C_ELABORATION_DIR bound to: ./ - type: string 
2default:defaulth p
x
� 
�
%s
*synth2j
V	Parameter C_COMPONENT_NAME bound to: fir_filter_fir_compiler_v7_2_i0 - type: string 
2default:defaulth p
x
� 
�
%s
*synth2i
U	Parameter C_COEF_FILE bound to: fir_filter_fir_compiler_v7_2_i0.mif - type: string 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_COEF_FILE_LINES bound to: 26 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_FILTER_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_INTERP_RATE bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_DECIM_RATE bound to: 1 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_ZERO_PACKING_FACTOR bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_SYMMETRY bound to: 1 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_NUM_FILTS bound to: 1 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_NUM_TAPS bound to: 51 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_NUM_CHANNELS bound to: 1 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_CHANNEL_PATTERN bound to: fixed - type: string 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_ROUND_MODE bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_COEF_RELOAD bound to: 0 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_NUM_RELOAD_SLOTS bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_COL_MODE bound to: 1 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_COL_PIPE_LEN bound to: 4 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_COL_CONFIG bound to: 26 - type: string 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_OPTIMIZATION bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_DATA_PATH_WIDTHS bound to: 16,16 - type: string 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_DATA_IP_PATH_WIDTHS bound to: 32 - type: string 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_DATA_PX_PATH_WIDTHS bound to: 32 - type: string 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_DATA_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_COEF_PATH_WIDTHS bound to: 16,16 - type: string 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_COEF_WIDTH bound to: 16 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_DATA_PATH_SRC bound to: 0,1 - type: string 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_COEF_PATH_SRC bound to: 0,0 - type: string 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_PX_PATH_SRC bound to: 0,1 - type: string 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter C_DATA_PATH_SIGN bound to: 1,0 - type: string 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter C_COEF_PATH_SIGN bound to: 0,0 - type: string 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter C_ACCUM_PATH_WIDTHS bound to: 35,34 - type: string 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter C_OUTPUT_WIDTH bound to: 50 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_OUTPUT_PATH_WIDTHS bound to: 50 - type: string 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter C_ACCUM_OP_PATH_WIDTHS bound to: 50 - type: string 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_EXT_MULT_CNFG bound to: 0,1,0,16 - type: string 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_DATA_PATH_PSAMP_SRC bound to: 0 - type: string 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_OP_PATH_PSAMP_SRC bound to: 0 - type: string 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_NUM_MADDS bound to: 26 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_OPT_MADDS bound to: none - type: string 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_OVERSAMPLING_RATE bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_INPUT_RATE bound to: 1 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_OUTPUT_RATE bound to: 1 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_DATA_MEMTYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_COEF_MEMTYPE bound to: 2 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_IPBUFF_MEMTYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_OPBUFF_MEMTYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_DATAPATH_MEMTYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter C_MEM_ARRANGEMENT bound to: 1 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_DATA_MEM_PACKING bound to: 0 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_COEF_MEM_PACKING bound to: 0 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_FILTS_PACKED bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_LATENCY bound to: 34 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_HAS_ARESETn bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_HAS_ACLKEN bound to: 1 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_DATA_HAS_TLAST bound to: 0 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter C_S_DATA_HAS_FIFO bound to: 0 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_S_DATA_HAS_TUSER bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S_DATA_TDATA_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_S_DATA_TUSER_WIDTH bound to: 1 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_M_DATA_HAS_TREADY bound to: 0 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_M_DATA_HAS_TUSER bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_M_DATA_TDATA_WIDTH bound to: 56 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_M_DATA_TUSER_WIDTH bound to: 1 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_HAS_CONFIG_CHANNEL bound to: 0 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_CONFIG_SYNC_MODE bound to: 0 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_CONFIG_PACKET_SIZE bound to: 0 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_CONFIG_TDATA_WIDTH bound to: 1 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_RELOAD_TDATA_WIDTH bound to: 1 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2(
fir_compiler_v7_2_102default:default2�
�e:/Bilibili_FPGA/N4_Project/VIVADO/hdl_netlist/fir_filter.srcs/sources_1/ip/fir_filter_fir_compiler_v7_2_i0/hdl/fir_compiler_v7_2_vh_rfs.vhd2default:default2
611412default:default2
U02default:default2(
fir_compiler_v7_2_102default:default2�
�e:/Bilibili_FPGA/N4_Project/VIVADO/hdl_netlist/fir_filter.srcs/sources_1/ip/fir_filter_fir_compiler_v7_2_i0/synth/fir_filter_fir_compiler_v7_2_i0.vhd2default:default2
2012default:default8@Z8-3491h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys23
fir_filter_fir_compiler_v7_2_i02default:default2
132default:default2
12default:default2�
�e:/Bilibili_FPGA/N4_Project/VIVADO/hdl_netlist/fir_filter.srcs/sources_1/ip/fir_filter_fir_compiler_v7_2_i0/synth/fir_filter_fir_compiler_v7_2_i0.vhd2default:default2
712default:default8@Z8-256h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized302default:default2
SCLR2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized302default:default2
SCLR_ALT2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2(
buff__parameterized02default:default2
RE2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2(
buff__parameterized02default:default2
SCLR2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2)
delay__parameterized22default:default2
SCLR_ALT2default:defaultZ8-3331h px� 
u
!design %s has unconnected port %s3331*oasys2
buff2default:default2
RE2default:defaultZ8-3331h px� 
w
!design %s has unconnected port %s3331*oasys2
buff2default:default2
SCLR2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized332default:default2
WE2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized332default:default2
CE2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized332default:default2
SCLR2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized332default:default2
SCLR_ALT2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized332default:default2
CLK2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized322default:default2
WE2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized322default:default2
CE2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized322default:default2
SCLR2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized322default:default2
SCLR_ALT2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized322default:default2
CLK2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2(
calc__parameterized02default:default2

PRE_ADDSUB2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2(
calc__parameterized02default:default2
CED2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized312default:default2
WE2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized312default:default2
CE2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized312default:default2
SCLR2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized312default:default2
SCLR_ALT2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized312default:default2
CLK2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2"
POUT[casc][52]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2"
POUT[casc][51]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2"
POUT[casc][50]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2"
POUT[casc][49]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2"
POUT[casc][48]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
P_EN2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
	OPCODE[9]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
	OPCODE[8]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
	OPCODE[7]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
	OPCODE[6]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
	OPCODE[5]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
	OPCODE[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
	OPCODE[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
	OPCODE[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
	OPCODE[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
	OPCODE[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[33]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[32]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[31]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[30]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[29]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[28]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[27]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[26]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[25]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[24]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[23]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[22]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[21]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[20]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[19]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[18]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[17]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[16]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[15]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[14]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[13]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[12]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[11]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[10]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[9]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[8]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[7]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[6]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[5]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2
CIN[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2!
PIN[casc][52]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2!
PIN[casc][51]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2!
PIN[casc][50]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2!
PIN[casc][49]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2!
PIN[casc][48]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2!
PIN[casc][47]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2!
PIN[casc][46]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2!
PIN[casc][45]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2!
PIN[casc][44]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2!
PIN[casc][43]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2!
PIN[casc][42]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2!
PIN[casc][41]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2!
PIN[casc][40]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2!
PIN[casc][39]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2!
PIN[casc][38]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2!
PIN[casc][37]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2!
PIN[casc][36]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2!
PIN[casc][35]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2!
PIN[casc][34]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2!
PIN[casc][33]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2!
PIN[casc][32]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2!
PIN[casc][31]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2!
PIN[casc][30]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2!
PIN[casc][29]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2!
PIN[casc][28]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
addsub_mult_add__parameterized22default:default2!
PIN[casc][27]2default:defaultZ8-3331h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-33312default:default2
1002default:defaultZ17-14h px� 
�
%s*synth2�
xFinished RTL Elaboration : Time (s): cpu = 00:01:38 ; elapsed = 00:01:43 . Memory (MB): peak = 650.184 ; gain = 346.832
2default:defaulth px� 
D
%s
*synth2,

Report Check Netlist: 
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished RTL Optimization Phase 1 : Time (s): cpu = 00:01:39 ; elapsed = 00:01:44 . Memory (MB): peak = 650.184 ; gain = 346.832
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
f
-Analyzing %s Unisim elements for replacement
17*netlist2
532default:defaultZ29-17h px� 
j
2Unisim Transformation completed in %s CPU seconds
28*netlist2
02default:defaultZ29-28h px� 
V
Loading part %s157*device2#
xc7z020clg400-22default:defaultZ21-403h px� 
K
)Preparing netlist for logic optimization
349*projectZ1-570h px� 
>

Processing XDC Constraints
244*projectZ1-262h px� 
=
Initializing timing engine
348*projectZ1-569h px� 
�
$Parsing XDC File [%s] for cell '%s'
848*designutils2�
�e:/Bilibili_FPGA/N4_Project/VIVADO/hdl_netlist/fir_filter.srcs/sources_1/ip/fir_filter_fir_compiler_v7_2_i0/fir_filter_fir_compiler_v7_2_i0_ooc.xdc2default:default2
U0	2default:default8Z20-848h px� 
�
-Finished Parsing XDC File [%s] for cell '%s'
847*designutils2�
�e:/Bilibili_FPGA/N4_Project/VIVADO/hdl_netlist/fir_filter.srcs/sources_1/ip/fir_filter_fir_compiler_v7_2_i0/fir_filter_fir_compiler_v7_2_i0_ooc.xdc2default:default2
U0	2default:default8Z20-847h px� 
�
$Parsing XDC File [%s] for cell '%s'
848*designutils2�
�e:/Bilibili_FPGA/N4_Project/VIVADO/hdl_netlist/fir_filter.srcs/sources_1/ip/fir_filter_fir_compiler_v7_2_i0/constraints/fir_compiler_v7_2.xdc2default:default2
U0	2default:default8Z20-848h px� 
�
-Finished Parsing XDC File [%s] for cell '%s'
847*designutils2�
�e:/Bilibili_FPGA/N4_Project/VIVADO/hdl_netlist/fir_filter.srcs/sources_1/ip/fir_filter_fir_compiler_v7_2_i0/constraints/fir_compiler_v7_2.xdc2default:default2
U0	2default:default8Z20-847h px� 
�
Parsing XDC File [%s]
179*designutils2�
uE:/Bilibili_FPGA/N4_Project/VIVADO/hdl_netlist/fir_filter.runs/fir_filter_fir_compiler_v7_2_i0_synth_1/dont_touch.xdc2default:default8Z20-179h px� 
�
Finished Parsing XDC File [%s]
178*designutils2�
uE:/Bilibili_FPGA/N4_Project/VIVADO/hdl_netlist/fir_filter.runs/fir_filter_fir_compiler_v7_2_i0_synth_1/dont_touch.xdc2default:default8Z20-178h px� 
H
&Completed Processing XDC Constraints

245*projectZ1-263h px� 
~
!Unisim Transformation Summary:
%s111*project29
%No Unisim elements were transformed.
2default:defaultZ1-111h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common24
 Constraint Validation Runtime : 2default:default2
00:00:012default:default2 
00:00:00.6312default:default2
878.3672default:default2
2.9382default:defaultZ17-268h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
~Finished Constraint Validation : Time (s): cpu = 00:02:34 ; elapsed = 00:02:42 . Memory (MB): peak = 878.367 ; gain = 575.016
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
V
%s
*synth2>
*Start Loading Part and Timing Information
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
J
%s
*synth22
Loading part: xc7z020clg400-2
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Loading Part and Timing Information : Time (s): cpu = 00:02:34 ; elapsed = 00:02:42 . Memory (MB): peak = 878.367 ; gain = 575.016
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Z
%s
*synth2B
.Start Applying 'set_property' XDC Constraints
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished applying 'set_property' XDC Constraints : Time (s): cpu = 00:02:34 ; elapsed = 00:02:43 . Memory (MB): peak = 878.367 ; gain = 575.016
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished RTL Optimization Phase 2 : Time (s): cpu = 00:02:40 ; elapsed = 00:02:49 . Memory (MB): peak = 878.367 ; gain = 575.016
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
$decloning instance '%s' (%s) to '%s'223*oasys2d
PU0/i_synth/g_single_rate.i_single_rate/g_parallel.g_cntrl[0].g_sym_cntrl.i_delay2default:default2
cntrl_delay2default:default2d
PU0/i_synth/g_single_rate.i_single_rate/g_parallel.g_cntrl[1].g_sym_cntrl.i_delay2default:defaultZ8-223h px� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Finished RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Y
%s
*synth2A
-Start RTL Hierarchical Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
[
%s
*synth2C
/Finished RTL Hierarchical Component Statistics
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2k
WPart Resources:
DSPs: 220 (col length:60)
BRAMs: 280 (col length: RAMB18 60 RAMB36 30)
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
W
%s
*synth2?
+Start Cross Boundary and Area Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2i
Ug_single_rate.i_single_rate/g_parallel.g_cntrl[24].g_sym_cntrl.g_reg.cntrl_reg[24][1]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2i
Ug_single_rate.i_single_rate/g_parallel.g_cntrl[23].g_sym_cntrl.g_reg.cntrl_reg[23][1]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2i
Ug_single_rate.i_single_rate/g_parallel.g_cntrl[22].g_sym_cntrl.g_reg.cntrl_reg[22][1]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2i
Ug_single_rate.i_single_rate/g_parallel.g_cntrl[21].g_sym_cntrl.g_reg.cntrl_reg[21][1]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2i
Ug_single_rate.i_single_rate/g_parallel.g_cntrl[20].g_sym_cntrl.g_reg.cntrl_reg[20][1]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2i
Ug_single_rate.i_single_rate/g_parallel.g_cntrl[19].g_sym_cntrl.g_reg.cntrl_reg[19][1]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2i
Ug_single_rate.i_single_rate/g_parallel.g_cntrl[18].g_sym_cntrl.g_reg.cntrl_reg[18][1]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2i
Ug_single_rate.i_single_rate/g_parallel.g_cntrl[17].g_sym_cntrl.g_reg.cntrl_reg[17][1]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2i
Ug_single_rate.i_single_rate/g_parallel.g_cntrl[16].g_sym_cntrl.g_reg.cntrl_reg[16][1]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2i
Ug_single_rate.i_single_rate/g_parallel.g_cntrl[15].g_sym_cntrl.g_reg.cntrl_reg[15][1]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2i
Ug_single_rate.i_single_rate/g_parallel.g_cntrl[14].g_sym_cntrl.g_reg.cntrl_reg[14][1]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2i
Ug_single_rate.i_single_rate/g_parallel.g_cntrl[13].g_sym_cntrl.g_reg.cntrl_reg[13][1]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2i
Ug_single_rate.i_single_rate/g_parallel.g_cntrl[12].g_sym_cntrl.g_reg.cntrl_reg[12][1]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2i
Ug_single_rate.i_single_rate/g_parallel.g_cntrl[11].g_sym_cntrl.g_reg.cntrl_reg[11][1]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2i
Ug_single_rate.i_single_rate/g_parallel.g_cntrl[10].g_sym_cntrl.g_reg.cntrl_reg[10][1]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2g
Sg_single_rate.i_single_rate/g_parallel.g_cntrl[9].g_sym_cntrl.g_reg.cntrl_reg[9][1]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2g
Sg_single_rate.i_single_rate/g_parallel.g_cntrl[8].g_sym_cntrl.g_reg.cntrl_reg[8][1]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2g
Sg_single_rate.i_single_rate/g_parallel.g_cntrl[7].g_sym_cntrl.g_reg.cntrl_reg[7][1]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2g
Sg_single_rate.i_single_rate/g_parallel.g_cntrl[6].g_sym_cntrl.g_reg.cntrl_reg[6][1]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2g
Sg_single_rate.i_single_rate/g_parallel.g_cntrl[5].g_sym_cntrl.g_reg.cntrl_reg[5][1]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2g
Sg_single_rate.i_single_rate/g_parallel.g_cntrl[4].g_sym_cntrl.g_reg.cntrl_reg[4][1]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2g
Sg_single_rate.i_single_rate/g_parallel.g_cntrl[3].g_sym_cntrl.g_reg.cntrl_reg[3][1]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2g
Sg_single_rate.i_single_rate/g_parallel.g_cntrl[2].g_sym_cntrl.g_reg.cntrl_reg[2][1]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2g
Sg_single_rate.i_single_rate/g_parallel.g_cntrl[1].g_sym_cntrl.g_reg.cntrl_reg[1][1]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2i
Ug_single_rate.i_single_rate/g_parallel.g_cntrl[25].g_sym_cntrl.g_reg.cntrl_reg[25][6]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2i
Ug_single_rate.i_single_rate/g_parallel.g_cntrl[25].g_sym_cntrl.g_reg.cntrl_reg[25][1]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2M
9g_single_rate.i_single_rate/g_parallel.flush_sym_data_reg2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2e
Qg_single_rate.i_single_rate/g_parallel.i_cntrl_sym_buff_sclr/gen_reg.d_reg_reg[0]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2�
�g_single_rate.i_single_rate/g_op_paths[0].g_combine.i_ext_mult/g_two_col_comb.i_delay_lower_pat_det/gen_dly.gen_shiftreg.gen_rtl_delay.delay_bus_reg[0][0]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2�
�g_single_rate.i_single_rate/g_op_paths[0].g_combine.i_ext_mult/g_two_col_comb.i_delay_lower_pat_det/gen_dly.gen_shiftreg.gen_rtl_delay.delay_bus_reg[1][0]2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
�
"merging instance '%s' (%s) to '%s'3436*oasys2n
ZU0/i_synth/g_single_rate.i_single_rate/g_m_data_chan_no_fifo.m_axis_data_tdata_int_reg[49]2default:default2
FDRE2default:default2n
ZU0/i_synth/g_single_rate.i_single_rate/g_m_data_chan_no_fifo.m_axis_data_tdata_int_reg[50]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2n
ZU0/i_synth/g_single_rate.i_single_rate/g_m_data_chan_no_fifo.m_axis_data_tdata_int_reg[50]2default:default2
FDRE2default:default2n
ZU0/i_synth/g_single_rate.i_single_rate/g_m_data_chan_no_fifo.m_axis_data_tdata_int_reg[51]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2n
ZU0/i_synth/g_single_rate.i_single_rate/g_m_data_chan_no_fifo.m_axis_data_tdata_int_reg[51]2default:default2
FDRE2default:default2n
ZU0/i_synth/g_single_rate.i_single_rate/g_m_data_chan_no_fifo.m_axis_data_tdata_int_reg[52]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2n
ZU0/i_synth/g_single_rate.i_single_rate/g_m_data_chan_no_fifo.m_axis_data_tdata_int_reg[52]2default:default2
FDRE2default:default2n
ZU0/i_synth/g_single_rate.i_single_rate/g_m_data_chan_no_fifo.m_axis_data_tdata_int_reg[53]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2n
ZU0/i_synth/g_single_rate.i_single_rate/g_m_data_chan_no_fifo.m_axis_data_tdata_int_reg[53]2default:default2
FDRE2default:default2n
ZU0/i_synth/g_single_rate.i_single_rate/g_m_data_chan_no_fifo.m_axis_data_tdata_int_reg[54]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2n
ZU0/i_synth/g_single_rate.i_single_rate/g_m_data_chan_no_fifo.m_axis_data_tdata_int_reg[54]2default:default2
FDRE2default:default2n
ZU0/i_synth/g_single_rate.i_single_rate/g_m_data_chan_no_fifo.m_axis_data_tdata_int_reg[55]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2l
XU0/i_synth/\g_single_rate.i_single_rate/g_m_data_chan_no_fifo.m_axis_data_tlast_int_reg 2default:defaultZ8-3333h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2_
Kg_single_rate.i_single_rate/g_m_data_chan_no_fifo.m_axis_data_tlast_int_reg2default:default2,
fir_compiler_v7_2_10_viv2default:defaultZ8-3332h px�
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Cross Boundary and Area Optimization : Time (s): cpu = 00:02:54 ; elapsed = 00:03:03 . Memory (MB): peak = 878.367 ; gain = 575.016
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
R
%s
*synth2:
&Start Applying XDC Timing Constraints
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Applying XDC Timing Constraints : Time (s): cpu = 00:03:30 ; elapsed = 00:03:41 . Memory (MB): peak = 890.176 ; gain = 586.824
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
F
%s
*synth2.
Start Timing Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
|Finished Timing Optimization : Time (s): cpu = 00:03:36 ; elapsed = 00:03:47 . Memory (MB): peak = 920.270 ; gain = 616.918
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-
Start Technology Mapping
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
{Finished Technology Mapping : Time (s): cpu = 00:04:31 ; elapsed = 00:04:43 . Memory (MB): peak = 957.609 ; gain = 654.258
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
?
%s
*synth2'
Start IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Q
%s
*synth29
%Start Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
T
%s
*synth2<
(Finished Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
uFinished IO Insertion : Time (s): cpu = 00:04:34 ; elapsed = 00:04:47 . Memory (MB): peak = 957.609 ; gain = 654.258
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
D
%s
*synth2,

Report Check Netlist: 
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Start Renaming Generated Instances
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Instances : Time (s): cpu = 00:04:35 ; elapsed = 00:04:47 . Memory (MB): peak = 957.609 ; gain = 654.258
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start Rebuilding User Hierarchy
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Rebuilding User Hierarchy : Time (s): cpu = 00:04:36 ; elapsed = 00:04:49 . Memory (MB): peak = 957.609 ; gain = 654.258
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Renaming Generated Ports
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Ports : Time (s): cpu = 00:04:36 ; elapsed = 00:04:49 . Memory (MB): peak = 957.609 ; gain = 654.258
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
M
%s
*synth25
!Start Handling Custom Attributes
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Handling Custom Attributes : Time (s): cpu = 00:04:37 ; elapsed = 00:04:49 . Memory (MB): peak = 957.609 ; gain = 654.258
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
J
%s
*synth22
Start Renaming Generated Nets
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Nets : Time (s): cpu = 00:04:37 ; elapsed = 00:04:49 . Memory (MB): peak = 957.609 ; gain = 654.258
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Writing Synthesis Report
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
A
%s
*synth2)

Report BlackBoxes: 
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
J
%s
*synth22
| |BlackBox name |Instances |
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
A
%s*synth2)

Report Cell Usage: 
2default:defaulth px� 
G
%s*synth2/
+------+----------+------+
2default:defaulth px� 
G
%s*synth2/
|      |Cell      |Count |
2default:defaulth px� 
G
%s*synth2/
+------+----------+------+
2default:defaulth px� 
G
%s*synth2/
|1     |DSP48E1   |     1|
2default:defaulth px� 
G
%s*synth2/
|2     |DSP48E1_1 |    52|
2default:defaulth px� 
G
%s*synth2/
|3     |LUT2      |    28|
2default:defaulth px� 
G
%s*synth2/
|4     |SRL16E    |  1376|
2default:defaulth px� 
G
%s*synth2/
|5     |SRLC32E   |   257|
2default:defaulth px� 
G
%s*synth2/
|6     |FDRE      |  2511|
2default:defaulth px� 
G
%s*synth2/
+------+----------+------+
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Writing Synthesis Report : Time (s): cpu = 00:04:37 ; elapsed = 00:04:49 . Memory (MB): peak = 957.609 ; gain = 654.258
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
t
%s
*synth2\
HSynthesis finished with 0 errors, 0 critical warnings and 530 warnings.
2default:defaulth p
x
� 
�
%s
*synth2�
~Synthesis Optimization Runtime : Time (s): cpu = 00:02:17 ; elapsed = 00:04:06 . Memory (MB): peak = 957.609 ; gain = 426.074
2default:defaulth p
x
� 
�
%s
*synth2�
Synthesis Optimization Complete : Time (s): cpu = 00:04:38 ; elapsed = 00:04:50 . Memory (MB): peak = 957.609 ; gain = 654.258
2default:defaulth p
x
� 
B
 Translating synthesized netlist
350*projectZ1-571h px� 
f
-Analyzing %s Unisim elements for replacement
17*netlist2
532default:defaultZ29-17h px� 
j
2Unisim Transformation completed in %s CPU seconds
28*netlist2
02default:defaultZ29-28h px� 
K
)Preparing netlist for logic optimization
349*projectZ1-570h px� 
u
)Pushed %s inverter(s) to %s load pin(s).
98*opt2
02default:default2
02default:defaultZ31-138h px� 
~
!Unisim Transformation Summary:
%s111*project29
%No Unisim elements were transformed.
2default:defaultZ1-111h px� 
U
Releasing license: %s
83*common2
	Synthesis2default:defaultZ17-83h px� 
�
G%s Infos, %s Warnings, %s Critical Warnings and %s Errors encountered.
28*	vivadotcl2
562default:default2
1002default:default2
02default:default2
02default:defaultZ4-41h px� 
^
%s completed successfully
29*	vivadotcl2 
synth_design2default:defaultZ4-42h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2"
synth_design: 2default:default2
00:04:462default:default2
00:04:592default:default2
957.6092default:default2
665.7302default:defaultZ17-268h px� 
�
 The %s '%s' has been generated.
621*common2

checkpoint2default:default2�
�E:/Bilibili_FPGA/N4_Project/VIVADO/hdl_netlist/fir_filter.runs/fir_filter_fir_compiler_v7_2_i0_synth_1/fir_filter_fir_compiler_v7_2_i0.dcp2default:defaultZ17-1381h px� 
�
,Added synthesis output to IP cache for IP %s415*coretcl2�
�e:/Bilibili_FPGA/N4_Project/VIVADO/hdl_netlist/fir_filter.srcs/sources_1/ip/fir_filter_fir_compiler_v7_2_i0/fir_filter_fir_compiler_v7_2_i0.xci2default:defaultZ2-1482h px� 
R
Renamed %s cell refs.
330*coretcl2
3642default:defaultZ2-1174h px� 
�
 The %s '%s' has been generated.
621*common2

checkpoint2default:default2�
�E:/Bilibili_FPGA/N4_Project/VIVADO/hdl_netlist/fir_filter.runs/fir_filter_fir_compiler_v7_2_i0_synth_1/fir_filter_fir_compiler_v7_2_i0.dcp2default:defaultZ17-1381h px� 
�
%s4*runtcl2�
�Executing : report_utilization -file fir_filter_fir_compiler_v7_2_i0_utilization_synth.rpt -pb fir_filter_fir_compiler_v7_2_i0_utilization_synth.pb
2default:defaulth px� 
�
sreport_utilization: Time (s): cpu = 00:00:00 ; elapsed = 00:00:00.139 . Memory (MB): peak = 957.609 ; gain = 0.000
*commonh px� 
�
Exiting %s at %s...
206*common2
Vivado2default:default2,
Mon Jul 27 12:05:28 20202default:defaultZ17-206h px� 


End Record