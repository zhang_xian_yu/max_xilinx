module IP_Receive(
	input 				G_CLK125M,		//GMII接收时钟 //125Mhz
	input 				RSTn,     		//清除/复位信号-总复位
	input		[7:0] 	G_DataIn, 		//GMII接收数据 === e_rxd
	input 				G_RXDV,   		//GMII接收数据有效信号
	//接收的MAC包信息
	output  	[47:0]  	Board_Mac,    //开发板端的MAC
	output  	[47:0]  	PC_Mac,	     //PC端的MAC 
	output  	[15:0] 	IP_Prtcl,     //IP 类型
	output          	Valid_ip_P,	  //接收IP类型值完成			
	//接收的IP包信息
	output  	[159:0] 	IP_Layer,     		//IP包头数据	
	output  	[15:0]  	Rx_Total_Len,   	//UDP frame的总长度	  
	output  	[31:0]  	PC_IP,        		//PC端的IP地址
	output  	[31:0]  	Board_IP,    		//开发板端的IP地址	
	//接收的UDP包信息
	output  	[63:0]  	UDP_Layer,    		//UDP包头		
	output  	[15:0]  	Rx_Data_Len,    	//接收的UDP数据包的长度 rx_data_length = rx_total_length - 20	
	//接收的UDP数据
	output  	[7:0]  	Udp_Data,       	//UDP接收的数据      //data_len = rx_data_length - 8      
	output          	Data_o_valid,     //UDP数据有效信号// 	
	output  	[10:0]   RAM_Wr_Addr,      //ram的写地址    //校验和也是算作UDP的数据
	//接收标志
	output	[3:0]   	Mark_Rx_State,		//接收状态机	 
	output          	Data_Receive    	//接收到UDP包标志			
	);
	module udp_rx(
    input                clk         ,    //时钟信号
    input                rst_n       ,    //复位信号，低电平有效
    
    input                gmii_rx_dv  ,    //GMII输入数据有效信号
    input        [7:0]   gmii_rxd    ,    //GMII输入数据
    output  reg          rec_pkt_done,    //以太网单包数据接收完成信号
    output  reg          rec_en      ,    //以太网接收的数据使能信号
    output  reg  [31:0]  rec_data    ,    //以太网接收的数据
    output  reg  [15:0]  rec_byte_num     //以太网接收的有效字数 单位:byte     
    );
	/************************参数初始化*******************************/
	//状态参数
	parameter 	Idle			=	4'd0,//4'b0000,
				Rx_Six55		=	4'd1,//4'b0001,
				Rx_d5			=	4'd2,//4'b0010,
				Rx_Mac			=	4'd3,//4'b0011,
				Rx_IP_Prtcl		=	4'd4,//4'b0100,
				Rx_IP_Layer		=	4'd5,//4'b0101,
				Rx_UDP_Layer	=	4'd6,//4'b0110,
				Rx_Data			=	4'd7,//4'b0111,
				Rx_Finish		=	4'd8,//4'b1000;
				Rx_wait_end    =    4'd9,//4'b1000;
					

reg 	[159:0] 	IP_Layer_r;		//IP 首部，共20*8=160字节
reg 	[63:0]  	UDP_Layer_r; 	//UDP 首部，共8*8=64字节
reg	[3:0]		Rx_State;

reg 	[47:0]  	Board_Mac_r;    //开发板端的MAC
reg 	[47:0]  	PC_Mac_r;	    //PC端的MAC 
reg 	[15:0] 	IP_Prtcl_r;     //IP 类型
reg         	Valid_ip_P_r;	 //接收IP类型值完成	

reg 	[15:0]  	Rx_Total_Len_r;   	//UDP frame的总长度	  
reg 	[31:0]  	PC_IP_r;     			//PC端的IP地址
reg 	[31:0]  	Board_IP_r;  			//开发板端的IP地址		
reg 	[15:0]  	Rx_Data_Len_r; 
reg 	[7:0]  	Udp_Data_r;       	//UDP接收的数据            
reg         	Data_o_valid_r;      //UDP数据有效信号// 	
reg 	[10:0]   RAM_Wr_Addr_r;       //ram的写地址    //校验和也是算作UDP的数据
reg         	Data_Receive_r;    	//接收到UDP包标志	 

/************************以太网包接收处理***************************/
reg	[4:0]		ByteCnt_r;
reg	[10:0]	DataCnt_r;
reg	[31:0]	DataTemp_r;

reg 	[95:0] 	Mac_Temp_r;	 		//6字节的源地址+6字节的目标地址=12B=96bit
reg 	[15:0] 	IP_Prtcl_Temp_r;  //2Byte
reg	[159:0] 	IP_Layer_Temp_r;  //20B=160bit
reg 	[63:0] 	UDP_Layer_Temp_r; //2（源端口）+2（目的端口）+2（长度）+2（校验和）=8B=64bit
		
always@(posedge G_CLK125M or negedge RSTn)begin
    if(!RSTn)begin
	   Rx_State			<=	Idle;
	   Data_Receive_r <= 1'b0;
	end
	else begin
	   case(Rx_State)
			Idle:begin
				Valid_ip_P_r	<=	1'b0;
				ByteCnt_r		<=	5'd0;
				DataCnt_r		<=	11'd0;
				Udp_Data_r		<=	8'h00;
				DataTemp_r		<= 32'h0000_0000;//存数组成32bit位宽，便于提取信息
				Data_o_valid_r	<=	1'b0; 
				RAM_Wr_Addr_r	<=	11'd0;               
				if((G_RXDV == 1'b1)&&(G_DataIn == 8'h55)) begin  //接收到第一个55        
                    Rx_State		<=	Rx_Six55;
                    DataTemp_r	<=	{DataTemp_r[23:0],G_DataIn}; //将8bit接收的数据， 转化为32bit位宽的数据
				end
				else begin
					Rx_State		<=	Idle;
				end
					//end	
				end
				Rx_Six55:
				begin
					if((G_RXDV == 1'b1)) begin
                        ByteCnt_r	<=	ByteCnt_r + 1'b1;
						if((ByteCnt_r < 5'd6) && (G_DataIn != 8'h55))  //7个8'h55  
                            Rx_State  <= Rx_wait_end;
					    else if(ByteCnt_r==5'd6) begin 
							ByteCnt_r	<=	5'd0;
							if(G_DataIn== 8'h55)
                                Rx_State		<=	Rx_Mac; 
                            else
                                Rx_State  <= Rx_wait_end;
						end
                        
					end
				Rx_Mac:
				begin
					if(G_RXDV == 1'b1)	
					begin
						if(ByteCnt_r < 5'd11)	//计数0~10（11是不成立），接收11字节
						begin
							Mac_Temp_r	<=	{Mac_Temp_r[87:0],G_DataIn[7:0]};
							ByteCnt_r	<=	ByteCnt_r + 1'b1;
						end
						else begin 	//最后一个字节在这里接收
							Board_Mac_r	<=	Mac_Temp_r[87:40];	//还有一个8bit数据要接收
							PC_Mac_r		<=	{Mac_Temp_r[39:0],G_DataIn[7:0]};//到这里，才刚好接收完！--觉得可以先接收下来，再判断
							ByteCnt_r	<=	5'd0;
							if((Mac_Temp_r[87:72] == 16'h000a)&&(Mac_Temp_r[71:56] == 16'h3501)&&(Mac_Temp_r[55:40] == 16'hfec0))//判断目标MAC Address是否为本FPGA
							begin
								Rx_State	<=	Rx_IP_Prtcl;//这里便是协议对接的地方，board_mac要下一时钟才完成更新
							end
							else begin
								Rx_State	<=	Idle;//如果接收的IP数据，不是发到本地的MAC地址，就不接收
							end
						end
					end
					else begin
						Rx_State	<=	Idle;
					end
				end
				Rx_IP_Prtcl:
				begin
					if(G_RXDV == 1'b1) 
					begin
						if(ByteCnt_r < 5'd1) 
						begin
							IP_Prtcl_Temp_r	<=	{IP_Prtcl_Temp_r[7:0],G_DataIn[7:0]};
							ByteCnt_r			<=	ByteCnt_r + 1'b1;
						end
						else begin
							IP_Prtcl_r		<=	{IP_Prtcl_Temp_r[7:0],G_DataIn[7:0]};
							Valid_ip_P_r	<=	1'b1;  //接收完成IP类型标志,待提取判断
							ByteCnt_r		<=	5'd0;
							Rx_State			<=	Rx_IP_Layer;
						end
					end
					else begin
						Rx_State	<=	Idle;
					end
				end
				Rx_IP_Layer:
				begin
					Valid_ip_P_r	<=	1'b0;//只产生一个脉冲
					if(G_RXDV == 1'b1) 
					begin
						if(ByteCnt_r < 5'd19)	
						begin
							IP_Layer_Temp_r	<=	{IP_Layer_Temp_r[151:0],G_DataIn[7:0]};
							ByteCnt_r			<=	ByteCnt_r + 1'b1;
						end
						else begin
							IP_Layer_r	<=	{IP_Layer_Temp_r[151:0],G_DataIn[7:0]};//存下来，并没有做判断
							ByteCnt_r	<=	5'd0;
							Rx_State		<=	Rx_UDP_Layer;
						end
					end
					else begin
						Rx_State	<=	Idle;
					end
				end
				Rx_UDP_Layer:
				begin
					Rx_Total_Len_r	<=	IP_Layer_r[143:128]; //提取IP包的总长度，其他信息未处理
					PC_IP_r			<=	IP_Layer_r[63:32];	//提取PC的IP地址
					Board_IP_r		<=	IP_Layer_r[31:0];		//本地IP地址
					//继续接收UDP数据
					if(G_RXDV == 1'b1) 
					begin
						if(ByteCnt_r < 5'd7)	
						begin
							UDP_Layer_Temp_r	<=	{UDP_Layer_Temp_r[55:0],G_DataIn};
							ByteCnt_r			<=	ByteCnt_r + 1'b1;
						end
						else begin
							UDP_Layer_r		<=	{UDP_Layer_Temp_r[55:0],G_DataIn};
							Rx_Data_Len_r	<= UDP_Layer_Temp_r[23:8];  //UDP数据包的总长度！						
							ByteCnt_r		<=	5'd0; //接收完8字节的UDP首部
							Rx_State			<=	Rx_Data;
						end
					end
					else begin
						Rx_State	<=	Idle;
					end
				end
				Rx_Data:
				begin
					if(G_RXDV == 1'b1) 
					begin
						if(DataCnt_r >= (Rx_Data_Len_r-9)) //真正的UDP数据需要减去8字节的UDP包头
						begin      
						//	DataCnt_r		<=	11'd0;
							Udp_Data_r		<= G_DataIn; //数据接口
							Data_o_valid_r	<=	1'b1;    
							RAM_Wr_Addr_r	<=	RAM_Wr_Addr_r + 1'b1; 	//控制RAM写入，与data_o_valid相伴而生		
  					      Rx_State			<=	Rx_Finish; 					//全部数据接受完！							
						end
						else begin
							DataCnt_r		<=	DataCnt_r + 1'b1;//写RAM
							Udp_Data_r		<=	G_DataIn;		
							Data_o_valid_r	<=	1'b1;                   //接受1Byes数据,写ram请求
							RAM_Wr_Addr_r	<=	RAM_Wr_Addr_r + 1'b1;	//写入一次加1					  
                  end							 
               end
					else begin
						Rx_State	<=	Idle;
					end
				end
				Rx_Finish:
				begin
					Data_o_valid_r		<=	1'b0;           
					Data_Receive_r		<=	1'b1; //用来检测是否成功接收到UDP数据。接收到的话，就把数据回显出来！所以不用清零了
					Rx_State				<=	Idle;
				end
				default:
				begin
					Rx_State	<=	Idle;
				end
			endcase
		end
	end
	assign	Mark_Rx_State	=	Rx_State;
	
	//接收的MAC包信息
	assign  	Board_Mac		=	Board_Mac_r;
	assign  	PC_Mac			=	PC_Mac_r; 
	assign  	IP_Prtcl			=	IP_Prtcl_r;   
	assign  	Valid_ip_P		=	Valid_ip_P_r;		
	//接收的IP包信息
	assign  	IP_Layer			=  IP_Layer_r;	
	assign  	Rx_Total_Len	=  Rx_Total_Len_r; 
	assign  	PC_IP				=	PC_IP_r;     
	assign  	Board_IP			=  Board_IP_r; 		
	//接收的UDP包信息
	assign  	UDP_Layer		=	UDP_Layer_r;	
	assign  	Rx_Data_Len		=	Rx_Data_Len_r;
	//接收的UDP数据
	assign  	Udp_Data			=  Udp_Data_r;               
	assign  	Data_o_valid	=	Data_o_valid_r;    	
	assign  	RAM_Wr_Addr		= 	RAM_Wr_Addr_r;     
	//接收标志
	assign	Mark_Rx_State	=	Rx_State;
	assign  	Data_Receive 	=  Data_Receive_r; 		
	
endmodule