//****************************************Copyright (c)***********************************//
//原子哥在线教学平台：www.yuanzige.com
//技术支持：www.openedv.com
//淘宝店铺：http://openedv.taobao.com 
//关注微信公众平台微信号："正点原子"，免费获取ZYNQ & FPGA & STM32 & LINUX资料。
//版权所有，盗版必究。
//Copyright(C) 正点原子 2018-2028
//All rights reserved	                               
//----------------------------------------------------------------------------------------
// File name:           ddr3_rw_top
// Last modified Date:  2019/8/21 9:56:36
// Last Version:        V1.0
// Descriptions:        读写测试实现模块
//----------------------------------------------------------------------------------------
// Created by:          正点原子
// Created date:        2019/8/21 10:55:56
// Version:             V1.0
// Descriptions:        The original version
//
//----------------------------------------------------------------------------------------
// Modified by:		    正点原子
// Modified date:	    2019/8/21 9:56:36
// Version:			    V1.0
// Descriptions:	    先往DDR3的若干连续地址中分别写入数据，再读出来进行比较，并输出比较结果
//
//----------------------------------------------------------------------------------------
//****************************************************************************************//

module ddr3_rw(          
    input                    ui_clk,                //用户时钟
    input                    ui_clk_sync_rst,       //复位,低有效
    input                    init_calib_complete,   //DDR3初始化完成
    input                    app_rdy,               //MIG IP核空闲
    input                    app_wdf_rdy,           //MIG写FIFO空闲
    input                    app_rd_data_valid,     //读数据有效
    input          [255:0]   app_rd_data,           //用户读数据
                             
    output  reg    [27:0]    app_addr,              //DDR3地址
    output                   app_en,                //MIG IP核操作使能
    output                   app_wdf_wren,          //用户写使能
    output                   app_wdf_end,           //突发写当前时钟最后一个数据 
    output         [2:0]     app_cmd,               //MIG IP核操作命令，读或者写
    output reg     [255:0]   app_wdf_data,          //用户写数据
    output reg     [1 :0]    state_cnt,             //状态计数器
    output reg     [23:0]    rd_addr_cnt,           //用户读地址计数
    output reg     [23:0]    wr_addr_cnt,           //用户写地址计数
    output reg     [20:0]    rd_cnt,                //实际读地址标记
    output reg               error_flag,
    output reg               led                    //读写测试结果指示灯
    );

//parameter define
parameter  TEST_LENGTH = 1000;
parameter  L_TIME = 49_000_000;
//reg define
reg  [30:0]  led_cnt;    //led计数
//wire define
wire         error;
 //*****************************************************
//**                    main code
//***************************************************** 

//读信号有效，且读出的数不是写入的数时，将错误标志位拉高
assign error = (app_rd_data_valid && (rd_cnt!=app_rd_data));

//在写状态MIG空闲且写有效,或者在读状态MIG空闲，此时使能信号为高，其他情况为低
assign app_en = ((state_cnt == 2'd1 && (app_rdy && app_wdf_rdy))
                ||(state_cnt == 2'd3 && app_rdy)) ? 1'b1:1'b0;
                
//在写状态,MIG空闲且写有效，此时拉高写使能
assign app_wdf_wren = (state_cnt == 2'd1 && (app_rdy && app_wdf_rdy)) ? 1'b1:1'b0;

//由于我们DDR3芯片时钟和用户时钟的分频选择4:1，突发长度为8，故两个信号相同
assign app_wdf_end = app_wdf_wren; 

//处于读的时候命令值为1，其他时候命令值为0
assign app_cmd = (state_cnt == 2'd3) ? 3'd1 :3'd0;  
    
//DDR3读写逻辑实现
always @(posedge ui_clk or negedge ui_clk_sync_rst) begin
    if((ui_clk_sync_rst)||(error_flag )) begin 
        state_cnt    <= 2'd0;          
        app_wdf_data <= 256'd0;     
        wr_addr_cnt  <= 24'd0;      
        rd_addr_cnt  <= 24'd0;       
        app_addr     <= 28'd0;          
    end
    else if(init_calib_complete)begin               //MIG IP核初始化完成
        case(state_cnt)
            0:begin
                state_cnt    <= state_cnt+1'd1;
                app_wdf_data <= 256'd0;   
                wr_addr_cnt  <= 24'd0;     
                rd_addr_cnt  <= 24'd0;       
                app_addr     <= 28'd0;        
             end
            1:begin
                if(wr_addr_cnt == TEST_LENGTH-1 &&(app_rdy && app_wdf_rdy))
                    state_cnt    <= state_cnt+1'd1;  //写到设定的长度跳到等待状态
                else if(app_rdy && app_wdf_rdy)begin //写条件满足
                    app_wdf_data <= app_wdf_data+1;  //写数据自加
                    wr_addr_cnt  <= wr_addr_cnt+1'd1;//写地址自加
                    app_addr     <= app_addr+ 8;     //DDR3 地址加8
                end
                else begin                           //写条件不满足，保持当前值
                    app_wdf_data <= app_wdf_data;      
                    wr_addr_cnt  <= wr_addr_cnt;
                    app_addr     <= app_addr; 
                end
              end
            2:begin                                                  
                state_cnt   <= state_cnt+1'd1;       //下一个时钟，跳到读状态
                rd_addr_cnt <= 24'd0;                //读地址复位
                app_addr    <= 28'd0;                //DDR3读从地址0开始
              end
            3:begin                                  //读到设定的地址长度    
                if(rd_addr_cnt == TEST_LENGTH-1 && app_rdy)
                    state_cnt   <= 2'd0;             //则跳到空闲状态 
                else if(app_rdy)begin                //若MIG已经准备好,则开始读
                    rd_addr_cnt <= rd_addr_cnt+1'd1; //用户地址每次加一
                    app_addr    <= app_addr+ 8;      //DDR3地址加8
                end
                else begin                           //若MIG没准备好,则保持原值
                    rd_addr_cnt <= rd_addr_cnt;
                    app_addr    <= app_addr; 
                end
              end
            default:begin
                state_cnt    <= 2'd0;
                app_wdf_data <= 256'd0;
                wr_addr_cnt  <= 24'd0;
                rd_addr_cnt  <= 24'd0;
                app_addr     <= 28'd0;
            end
        endcase
    end
end   
                        
//对DDR3实际读地址的编号计数
always @(posedge ui_clk or negedge ui_clk_sync_rst) begin
    if(ui_clk_sync_rst) 
        rd_cnt  <= 0;              //若计数到读写长度，且读有效，地址计数器则置0                                    
    else if(app_rd_data_valid && rd_cnt == TEST_LENGTH-1)
         rd_cnt <= 0;              //其他条件只要读有效，每个时钟自增1
    else if (app_rd_data_valid )
        rd_cnt <= rd_cnt+1;
end

always @(posedge ui_clk or negedge ui_clk_sync_rst) begin
    if(ui_clk_sync_rst) 
        error_flag <= 0;
    else if(error)
        error_flag <= 1;
 end
 
always @(posedge ui_clk or negedge ui_clk_sync_rst) begin
    if(ui_clk_sync_rst) begin
        led_cnt <= 31'd0;
        led <= 1'b0;
    end
    else begin
        if(~error_flag)                        //读写测试正确         
            led <= 1'b1;                       //led灯常亮
         else begin                            //读写测试错误
            led_cnt <= led_cnt + 31'd1;
            if(led_cnt == 49_000_000 - 1'b1) begin
            led_cnt <= 31'd0;
            led <= ~led;                   //led灯闪烁
            end                    
         end
      end
end

endmodule
