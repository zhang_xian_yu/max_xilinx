//****************************************Copyright (c)***********************************//
//原子哥在线教学平台：www.yuanzige.com
//技术支持：www.openedv.com
//淘宝店铺：http://openedv.taobao.com 
//关注微信公众平台微信号："正点原子"，免费获取ZYNQ & FPGA & STM32 & LINUX资料。
//版权所有，盗版必究。
//Copyright(C) 正点原子 2018-2028
//All rights reserved	                               
//----------------------------------------------------------------------------------------
// File name:           ddr3_rw_top
// Last modified Date:  2019/8/21 9:56:36
// Last Version:        V1.0
// Descriptions:        DDR3读写测试顶层
//----------------------------------------------------------------------------------------
// Created by:          正点原子
// Created date:        2019/8/21 10:55:56
// Version:             V1.0
// Descriptions:        The original version
//
//----------------------------------------------------------------------------------------
// Modified by:		    正点原子
// Modified date:	    2019/8/21 9:56:36
// Version:			    V1.0
// Descriptions:	    先往DDR3 的若干连续地址中分别写入数据，再读出来进行比较
//
//----------------------------------------------------------------------------------------
//****************************************************************************************//

module ddr3_rw_top(
   input              sys_clk,         //系统时钟
   input              sys_rst_n,       //复位,低有效
    // DDR3
   inout [31:0]       ddr3_dq,         //DDR3 数据
   inout [3:0]        ddr3_dqs_n,      //DDR3 dqs负
   inout [3:0]        ddr3_dqs_p,      //DDR3 dqs正  
        
   output [13:0]      ddr3_addr,       //DDR3 地址
   
   output [2:0]       ddr3_ba,         //DDR3 banck 选择
   output             ddr3_ras_n,      //DDR3 行选择
   output             ddr3_cas_n,      //DDR3 列选择
   output             ddr3_we_n,       //DDR3 读写选择
   output             ddr3_reset_n,    //DDR3 复位
   output [0:0]       ddr3_ck_p,       //DDR3 时钟正
   output [0:0]       ddr3_ck_n,       //DDR3 时钟负
   output [0:0]       ddr3_cke,        //DDR3 时钟使能
   output [0:0]       ddr3_cs_n,       //DDR3 片选
   output [3:0]       ddr3_dm,         //DDR3_dm
   output [0:0]       ddr3_odt,        //DDR3_odt
   //用户
   output             led       //错误指示信号
    );                
                      
 //wire define  
wire                  ui_clk ;             //用户时钟
wire                  error_flag;
wire [27:0]           app_addr;            //DDR3 地址
wire [2:0]            app_cmd;             //用户读写命令
wire                  app_en;              //MIG IP核使能
wire                  app_rdy;             //MIG IP核空闲
wire [255:0]          app_rd_data;         //用户读数据
wire                  app_rd_data_end;     //突发读当前时钟最后一个数据 
wire                  app_rd_data_valid;   //读数据有效
wire [255:0]          app_wdf_data;        //用户写数据 
wire                  app_wdf_end;         //突发写当前时钟最后一个数据 
wire [31:0]           app_wdf_mask;        //写数据屏蔽
wire                  app_wdf_rdy;         //写空闲
wire                  app_sr_active;       //保留
wire                  app_ref_ack;         //刷新请求
wire                  app_zq_ack;          //ZQ 校准请求
wire                  app_wdf_wren;        //DDR3 写使能                  
wire                  locked;              //锁相环频率稳定标志
wire                  clk_ref_i;           //DDR3参考时钟
wire                  sys_clk_i;           //MIG IP核输入时钟
wire                  clk_200;             //200M时钟
wire                  ui_clk_sync_rst;     //用户复位信号
wire                  init_calib_complete; //校准完成信号
wire [20:0]           rd_cnt;              //实际读地址计数
wire [1 :0]           state_cnt;           //状态计数器
wire [23:0]           rd_addr_cnt;         //用户读地址计数器
wire [23:0]           wr_addr_cnt;         //用户写地址计数器

//*****************************************************
//**                    main code
//*****************************************************

//读写模块
 ddr3_rw u_ddr3_rw(
    .ui_clk               (ui_clk),                
    .ui_clk_sync_rst      (ui_clk_sync_rst),       
    .init_calib_complete  (init_calib_complete),
    .app_rdy              (app_rdy),
    .app_wdf_rdy          (app_wdf_rdy),
    .app_rd_data_valid    (app_rd_data_valid),
    .app_rd_data          (app_rd_data),
    
    .app_addr             (app_addr),
    .app_en               (app_en),
    .app_wdf_wren         (app_wdf_wren),
    .app_wdf_end          (app_wdf_end),
    .app_cmd              (app_cmd),
    .app_wdf_data         (app_wdf_data),
    .state_cnt            (state_cnt),
    .rd_addr_cnt          (rd_addr_cnt),
    .wr_addr_cnt          (wr_addr_cnt),
    .rd_cnt               (rd_cnt),
    .error_flag           (error_flag),
    .led                  (led)
    );
    
//MIG IP核模块
mig_7series_0 u_mig_7series_0 (
    // Memory interface ports
    .ddr3_addr                      (ddr3_addr),   // output [14:0]	ddr3_addr
    .ddr3_ba                        (ddr3_ba),     // output [2:0]	ddr3_ba
    .ddr3_cas_n                     (ddr3_cas_n),  // output		ddr3_cas_n
    .ddr3_ck_n                      (ddr3_ck_n),   // output [0:0]	ddr3_ck_n
    .ddr3_ck_p                      (ddr3_ck_p),   // output [0:0]	ddr3_ck_p
    .ddr3_cke                       (ddr3_cke),    // output [0:0]	ddr3_cke
    .ddr3_ras_n                     (ddr3_ras_n),  // output		ddr3_ras_n
    .ddr3_reset_n                   (ddr3_reset_n),// output		ddr3_reset_n
    .ddr3_we_n                      (ddr3_we_n),   // output		ddr3_we_n
    .ddr3_dq                        (ddr3_dq),     // inout [31:0]	ddr3_dq
    .ddr3_dqs_n                     (ddr3_dqs_n),  // inout [3:0]	ddr3_dqs_n
    .ddr3_dqs_p                     (ddr3_dqs_p),  // inout [3:0]	ddr3_dqs_p
    .init_calib_complete            (init_calib_complete), 
                                                   // init_calib_complete
	.ddr3_cs_n                      (ddr3_cs_n),   // output [0:0]	ddr3_cs_n
    .ddr3_dm                        (ddr3_dm),     // output [3:0]	ddr3_dm
    .ddr3_odt                       (ddr3_odt),    // output [0:0]	ddr3_odt
    // Application interface ports
    .app_addr                       (app_addr),    // input [28:0]	app_addr
    .app_cmd                        (app_cmd),     // input [2:0]	app_cmd
    .app_en                         (app_en),      // input			app_en
    .app_wdf_data                   (app_wdf_data),// input [255:0] app_wdf_data
    .app_wdf_end                    (app_wdf_end), // input         app_wdf_end
    .app_wdf_wren                   (app_wdf_wren),// input	        app_wdf_wren
    .app_rd_data                    (app_rd_data), // output [255:0]app_rd_data
    .app_rd_data_end                (app_rd_data_end),  
                                                   // output	    app_rd_data_end
    .app_rd_data_valid              (app_rd_data_valid),  
                                                   // output	    app_rd_data_valid
    .app_rdy                        (app_rdy),     // output	    app_rdy
    .app_wdf_rdy                    (app_wdf_rdy), // output	    app_wdf_rdy
    .app_sr_req                     (),            // input	        app_sr_req
    .app_ref_req                    (),            // input	        app_ref_req
    .app_zq_req                     (),            // input	        app_zq_req
    .app_sr_active                  (app_sr_active),// output	    app_sr_active
    .app_ref_ack                    (app_ref_ack),  // output	    app_ref_ack
    .app_zq_ack                     (app_zq_ack),   // output	    app_zq_ack
    .ui_clk                         (ui_clk),       // output	    ui_clk
    .ui_clk_sync_rst                (ui_clk_sync_rst), 
                                                    // output       ui_clk_sync_rst
    .app_wdf_mask                   (31'b0),        // input [31:0]	app_wdf_mask
    // System Clock Ports
    .sys_clk_i                       (clk_200),
    // Reference Clock Ports
    .clk_ref_i                      (clk_200),
    .sys_rst                        (sys_rst_n)     // input         sys_rst
    );
    
//PLL模块 
clk_wiz_0 instance_name
   (
    // Clock out ports
    .clk_out1(clk_200),     // output clk_out1
    // Status and control signals
    .reset(1'b0),           // input resetn
    .locked(locked),        // output locked
   // Clock in ports
    .clk_in1(sys_clk)
    );                      // input clk_in1
    
//调试模块 
ila_0 your_instance_name (
	.clk(ui_clk),                  // input wire [0:0]    clk
	.probe0(app_wdf_data),         // input wire [255:0]  probe0  
	.probe1(app_cmd),              // input wire [2:0]  probe1 
	.probe2(app_addr),             // input wire [28:0]  probe2 
	.probe3(app_wdf_wren),         // input wire [0:0]  probe3 
	.probe4(app_en),               // input wire [0:0]  probe4 
	.probe5(app_wdf_rdy),          // input wire [0:0]  probe5 
	.probe6(app_rdy) ,             // input wire [0:0]  probe6
	.probe7(app_rd_data),          // input wire [255:0]  probe7 
	.probe8(state_cnt),            // input wire [1:0]  probe8 
	.probe9(app_rd_data_valid),    // input wire [0:0]  probe9
	.probe10(wr_addr_cnt),         // input wire [23:0]  probe10 
	.probe11(rd_addr_cnt),         // input wire [23:0]  probe11
	.probe12(rd_cnt),              // input wire [20:0]  probe12
	.probe13(init_calib_complete) ,// input wire [0:0]  probe13
	.probe14(error_flag)           // input wire [0:0]  probe14
   );

endmodule