// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "ila,Vivado 2018.3" *)
module ila_0(clk, probe0, probe1, probe2, probe3, probe4, probe5, 
  probe6, probe7, probe8, probe9, probe10, probe11, probe12, probe13, probe14);
  input clk;
  input [255:0]probe0;
  input [2:0]probe1;
  input [28:0]probe2;
  input [0:0]probe3;
  input [0:0]probe4;
  input [0:0]probe5;
  input [0:0]probe6;
  input [255:0]probe7;
  input [1:0]probe8;
  input [0:0]probe9;
  input [23:0]probe10;
  input [23:0]probe11;
  input [20:0]probe12;
  input [0:0]probe13;
  input [0:0]probe14;
endmodule
